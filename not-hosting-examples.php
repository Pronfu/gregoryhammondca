<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Not Hosting Examples Anymore | Gregory Hammond </title>
  <meta name="description" content="I'm no longer posting any live demos to my website.">

  <?php include('header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Not Hosting Examples / Live Demos Anymore </h3>
        <p>
         For many years I was hosting examples / live demos of things I created on this website. This was so people could see a live demo and decide if they wanted to put it on their own site. 
         <br> <br>
         Recently I moved all my open-source stuff to <a href="https://bitbucket.org/Pronfu/">Bitbucket</a> which makes it very easy for people to clone my repos as they want.
        <br>
        <?php include('footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->