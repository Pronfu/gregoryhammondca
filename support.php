<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Support the site | Gregory Hammond </title>
  <meta name="description" content="gregoryhammond.ca has no analystics, paywalls, or anything to know who you are. Can you help keep the site running?">

  <?php include('header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Support The Site </h3>
        <p>
         When creating this site I decided to not put analytics, paywalls, ads or anything else that could track people on this site. In doing that, it leaves me to foot the bill. I will run this site forever but I would appreciate financial support.
         <br> <br>
         It costs about $120 CAD per year to run this website, and I would appreciate financial support to run this site. My time in running the site (writing posts, keeping the site updated etc..) isn't included in the cost.
         <br> <br>
         If I receive anything above the cost of running the site (for the year) I will keep it as surplus, if I have a huge surplus then I will remove this page.

         <br> <br>
         If you need help with your website, then please consider hiring my company (<a href="https://gjdev.ca?ref=gregoryhammondcasupport">Gregory J Development</a>), as it's just me right now all profits go back to me / my company.

         <br> <br>
         <u> Direct Support </u>
         <br> <br>
         <a href="https://paypal.me/ghammondca">Paypal</a> (no fees since the link is for "friends and family"): <a href="https://paypal.me/ghammondca">paypal.me/ghammondca</a>
		 <br>
		 <a href="https://www.buymeacoffee.com/gregoryhca">Buy Me A Coffee</a>

         <br> <br>
         <u>E-Gift Card</u>, think of it like buy me a coffee, or a book. If you want to send a different e-gift card you are welcome to. Please send all e-gift cards to egift@gregoryhammond.ca
         <br> <br>
         <a href="https://www.starbucks.ca/shop/card/egift">Starbucks E-Gift Card</a> (accepted amount between $5 - $100)
         <br> <br>
         <a href="https://www.chapters.indigo.ca/en-ca/giftcards">Indigo / Chapters E-Gift Card</a> (accepted amount from $2 - $500)
         <br> <br>
         <a href="https://www.amazon.ca/dp/B01FRG9CKA/ref=s9_acss_bw_cg_GCOCLS1_1a1_w?pf_rd_m=A1IM4EOPHS76S7&pf_rd_s=merchandised-search-3&pf_rd_r=AVS1442G33ZYYPYMCD9N&pf_rd_t=101&pf_rd_p=66536961-394f-4ac5-9ad9-b88b6151a445&pf_rd_i=9230166011">Amazon.ca E-Gift Card</a> (accepted amount from $0.15 to $2,000)

		 
		 <br> <br>
		 <u> Cryptocurrency</u>
		 <br> <br>
		 BTC: <a href="bitcoin:3K1yudLAuZXjtAEanuZn5nN96vQRSt78zN">3K1yudLAuZXjtAEanuZn5nN96vQRSt78zN</a>
		 <br> <br>
		 LTC: <a href="litecoin:MNezSqYnjHqrUqGVz8nmXrQvSPwZdFq43C">MNezSqYnjHqrUqGVz8nmXrQvSPwZdFq43C</a>
		 <br> <br>
		 <form action="https://www.coinpayments.net/index.php" method="post">
	      <input type="hidden" name="cmd" value="_donate">
	      <input type="hidden" name="reset" value="1">
	      <input type="hidden" name="merchant" value="acb1bd3409831e12b8fb06f7cf9c6ecc">
	      <input type="hidden" name="item_name" value="Support for gregoryhammond.ca">
	      <input type="hidden" name="item_number" value="5000">
	      <input type="hidden" name="invoice" value="0001">
	      <input type="hidden" name="currency" value="CAD">
	      <input type="hidden" name="amountf" value="5.00000000">
	      <input type="hidden" name="allow_amount" value="1">
	      <input type="hidden" name="want_shipping" value="0">
	      <input type="hidden" name="allow_extra" value="1">
	      <input type="image" src="https://res.cloudinary.com/doklvodcx/image/upload/f_auto,q_auto/v1539047598/donate-med-grey_yjvmt2.png" alt="Donate with CoinPayments.net">
         </form>
         <br> <br>
         <u>Referral Links</u>, signup using the link and you get something, and I get a percentage back to me)
         <br> <br>
         <a href="https://www.cloudways.com/en/?id=338165">Cloudways</a> (I receive $50 if you sign up and deposit some money).
		 <br> <br>
         <a href="https://cloudinary.com/invites/lpov9zyyucivvxsnalc5/atgngbnswtfuaadqe4eb">Cloudinary</a> (cdn for almost anything, just signup for free and I receive 1,000 MB of storage & 2,000 MB of bandwidth in my account to use)
         <br> <br>
         <a href="http://depositphotos.com?ref=14478140">Depositphotos</a> (I receive 15% if you buy a pay-per-credit or on demand, I recieve somewhere between 4 - 10% if you buy a subscription plan)
		 <br> <br>
		 <a href="https://db.tt/XPBiL4BGkX">Dropbox</a> (I get 500mb for free when you signup using my referral link)
		 <br> <br>
         <a href="http://fut.io/a?f852d19a33">FolowUpThen</a> (You get a $5 credit, I receive $5 if you become a paying customer)
         <br> <br>
		 <a href="https://share.kobo.com/x/THnVRO">Kobo</a> (You get $5 off your first eBook, I get $10 in Kobo Credit)
		 <br> <br>
		 <a href="https://www.lyft.com/invite/RELAXCOMFY">Lyft</a> (You get $20 in Lyft credit, I get $50 in Lyft Credit)
		 <br> <br>
         <a href="https://www.namesilo.com/?rid=a0f3725kt">NameSilo</a> (I get 10% from your first purchase)
         <br> <br>
         <a href="https://shareasale.com/r.cfm?b=568425&u=1667705&m=51809&urllink=&afftrack=">SeedProd</a> (I get 30% of each sale)
         <br> <br>
		 <a href="https://shortpixel.com/free-sign-up-referrer/referrer/378733">ShortPixel</a> (You get to optimize 200 images for free, I get an additional 100 images to optimize)
		 <br> <br>
         <a href="https://thathost.ca/billing/aff.php?aff=3">That Hosting Company</a>
		 <br> <br>
		 <a href="https://refer.quickbooks.ca/s/gregory41">Quickbooks Online</a> (You get a 50% discount for the first 6 months, I get $150 visa prepaid gift card)
		 <br> <br>
		 <a href="https://auth.uber.com/login/?uber_client_name=riderSignUp&promo_code=gregoryh4768ue&">Uber</a> (Get $5 off your first ride, I get a free ride worth up to $5)
		 <br> <br>
         <a href="
https://join.worldcommunitygrid.org?recruiterId=993391&teamId=G7H7GNTHP1">World Community Grid</a> (uses your spare computing power to help develop new drugs to save lives, I get a warm fuzzy feeling if you sign up)
        <br>
        <?php include('footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->