<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Security vs Convenience | Gregory Hammond</title>
  <meta name="description" content="When a site says remember me. What does it actually do, and how does it affect you.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Security vs Convenience </h3>
        <p>
         Do you want to be able to open a website and be logged in, or have to log in each time you open a site? Want to have every site you visit track you, or have nobody track you? These are the two things to look into when you want security or convenience.
         <br> <br>
         If I go to facebook right now then I will have to login, and enter my two-factor code. It may take me more time (including removing my cookies after I visit the site) but I know facebook isn't tracking me. Now I'm set to be logged in on apps like facebook on my phone (not using the <a href="https://thenextweb.com/contributors/2017/08/11/big-brother-name-facebook/#.tnw_BoW9CAsd"official facebook app</a> since it is known to <a href="http://www.forbes.com/sites/kashmirhill/2014/05/22/facebook-wants-to-listen-in-on-what-youre-doing/">listen to you</a>, <a href="https://www.theregister.co.uk/2015/11/10/facebook_scans_camera_for_your_friends/">take and scan pictures</a>, <a href="https://www.tripwire.com/state-of-security/security-data-protection/cyber-security/stalk-location-facebook-messenger/">track your location</a> and <a href="https://stallman.org/facebook.html">many more things</a>) but I know that it is convenient.
         <br> <br>
         So think about it the next time a website says remember me.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->