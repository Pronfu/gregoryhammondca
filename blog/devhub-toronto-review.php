<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Devhub Toronto Review | Gregory Hammond </title>
  <meta name="description" content="This is my review of Devhub Toronto and why I won't be going back.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Devhub Toronto Review </h3>
        <p>
        <i> Date published: October 7th 2018 </i>
		<br>
		<i> Read Time: 3 minutes 49 seconds </i>
        <br> <br>
        I got the chance to have a one-day free trial at <a href="http://devhub.ca/about-us/toronto/">Devhub Toronto</a>. I got a seat in their hotdesk section from 10am to 5pm. Here is my experience and why I may not use a desk there again. I'm not going to go into detail on timing and exactly where I sat as more <a href="https://en.wikipedia.org/wiki/Open-source_intelligence">OSINT</a> on me isn't needed.
		<br> <br>
		I got there in the morning and I though I would need to call them since they didn't tell me how to get in (do I need to enter a certain code on the intercom to be let in or do I open the door). I was lucky and someone came behind me soon after I got there and opened the door. 
		<br> <br>
		Once I got up the 4 flights of stairs (once you get in there are a set of stairs up or down) I got to Devhub (it says Lighthouse Labs on the signs inside the front doors) there wasn't anyone at the reception. It would have been nice to have someone at the reception to show me around (I was told in emails that there would be someone but I didn't see them until the afternoon).
		<br> <br>
		Once I found a place to sit I decided to get to work, there are signs that tell the wifi networks and the respective passwords. Before working I decided to check the wifi speed, I used two different speed test websites (note: speed test websites aren't 100% accurate as noted by <a href="https://www.kirsle.net/my-isp-is-cheating-on-speed-tests">Noah Petherbridge</a> and <a href="https://tomrichards.net/2014/04/my-isp-is-cheating-on-speed-tests-too/">Tom Richards</a>):
		<br> <br>
		<a href="http://www.speedtest.net/">Speedtest.net</a> (Toronto Server) - 64.19mbps download , 102.09mbps upload
		<br>
		<a href="https://speedof.me/">Speedof.me</a> (London Server) - 1.77mbps download, 0.98mbps upload
		<br> <br>
		So as you can see the Toronto speed test have good speeds, the London one not so much but all in all still very good speed for lots of people using it. I was able to connect to my vpn, there are so many places that block vpn's it was nice to have a place that didn't block it. I worked for a while until I decided to have lunch, since I didn't have a lunch with me.
		<br> <br>
		After lunch I went back and there happened to be someone of ahead of me going in, and someone getting out of the building. So perfect timing. I noticed that in the afternoon it got a lot busier which means noiser which means it's harder to get work done.
		<br> <br>
		In the middle of the afternoon I was going to take a break and get some tea (since the <a href="http://devhub.ca/pricing/">devhub website promotes unlimited coffee and tea</a>) but I searched through the small kitchen and couldn't find any mugs or any tea bags. While I didn't ask for any help, I felt there should be signs or something to show people where mugs are, there tea bags can be found or even tell people to bring their own waterbottle / mug. 
		<br> <br>
		I worked until the late afternoon when an event started which was an open house and since the floor is so open it was very hard for me to work. I decided to pack up and go to a nearby coffee shop to finish my work.
		<br> <br>
		The big thing that I noticed was that it was a very open space, the only walls were the walls to the outside and the walls for a private office. If you don't have a private office then you have to work in the same area as everyone else which can be distracting and noisy. Devhub promots the place as a quiet place to work, but I found the opposite. It was noisy, distracting and not a place if you need to focus.
		<br> <br>
		A thing that many co-working spaces have, but Devhub didn't seem to have is a place to take phone calls. If you get a phone call you will have to answer it and unless you want to be rude to the people around you tell the caller to send you an email.
		<br> <br>
		Something that many people may not like is the old floors, they can be distracting and annoying if people are walking close to you. Sometimes old buildings are cool and a great place to work but it was hard to work in this place and I won't work here again.
		<br> <br>
		To summarize, contact devhub and try out a one-day free trial to see if their co-working space is right for you. Devhub didn't work for me, but I will go out other places (like co-working spaces and coffee shops) to find a place outside of my office that will work for me.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->