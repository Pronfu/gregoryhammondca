<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Too Many Methods Of Communication | Gregory Hammond </title>
  <meta name="description" content="There are so many ways to talk to people duirng this day and ago. Is there too many?">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Too Many Methods Of Communication </h3>
        <p>
        We live in this time where there are many ways to talk to people some for example have been around for a while like phone calls, text messages and emails.
        <br>
        But in the last 10 years or so the communication field has exploded, there are now apps for workplace talk (slack, facebook for work etc...) and way too many ways to personal talk (facebook message, tweet, instagram post/message, snapchat post/message, iMessage etc...)
	   <br> <br>
	   Now I like having different platforms and I use them for different things, but now I just have it setup to email me every time I get a message on a platform so I don't have to keep all these sites open, but an email inbox can quickly become overwhelming.
       <br> <br>
       Lets get back to basics and use only a small handful of platforms to communicate on.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->