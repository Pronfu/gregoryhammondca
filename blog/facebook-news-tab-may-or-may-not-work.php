<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Facebook News Tab: Why It May Or May Not Work| Gregory Hammond </title>
  <meta name="description" content="Facebook will be adding a news tab to select mobile US users. It may work in favor of Facebook or it may not.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3>Facebook News Tab: Why It May Or May Not Work</h3>
        <p>
        <i> Date published: October 26, 2019 </i>
        <br> <br>
        On October 25th 2019 Facebook <a href="https://newsroom.fb.com/news/2019/10/introducing-facebook-news/">announced Facebook news</a> (really just a new tab in Facebook titled news). It is interesting that Facebook is announcing this, there are some reasons why I believe it will work and some reasons why it won't work (and why publications shouldn't publish there). I will be doing my best to provide more details with links to both left and right leaning websites. Although it might not be a perfect balance due to the fact that people sometimes label a website with a bias to one side (or another) based on just a couple of stories, ultimately the search for a website bias came from <a href="https://mediabiasfactcheck.com">mediabiasfactcheck.com</a>.
		<br> <br>
		First why it may work, Facebook has around 2.41 billion momthly active users as of June 30, 2019 (according to <a href="https://newsroom.fb.com/company-info/">Facebook</a>) and many of those people check Facebook multiple times per day. It shows just the reach of Facebook. It also allows publications to be read by people who normally wouldn't read them (or are a fan of that publication but wouldn't go to their direct website). Facebook is also paying publications to publish there (according to <a href="https://www.wsj.com/articles/facebook-offers-news-outlets-millions-of-dollars-a-year-to-license-content-11565294575?mod=rsswn">Wall Street Journal</a> and <a href="https://www.bloomberg.com/news/articles/2019-10-25/facebook-creates-news-section-to-compensate-restive-publishers">Bloomberg</a>). Facebook is also allowing a user to easily click on the headline and go directly to the publishers website or app (according to <a href="https://www.apnews.com/95fc841e4f3f4b0ab4414cbd9719acf5">Associated Press</a>).
		<br> <br>
		Now here is why I believe it may not work, it's currently available only in the US to select users (according to <a href="https://www.politico.com/newsletters/morning-tech/2019/10/25/the-read-on-facebooks-news-tab-781529">Politico</a> and <a href="https://www.marketwatch.com/story/facebook-unveils-new-tab-in-its-mobile-app-dedicated-to-news-stories-2019-10-25">MarketWatch</a>) and on mobile devices (according to <a href="https://www.pocket-lint.com/apps/news/facebook/149902-what-is-facebook-news-and-how-does-it-work">Pocket-lint</a>,  <a href="https://www.ocregister.com/2019/10/25/facebook-launches-a-news-section-and-will-pay-publishers/">The Orange County Register</a>, and <a href="https://www.nytimes.com/2019/08/20/technology/facebook-news-humans.html">New York Times</a>). On the sub-topic of money, Facebook has said that they will be paying only select publications (according to <a href="https://www.ft.com/content/ba031844-f3e3-11e9-a79c-bc9acae3b654">Financial Times</a>, <a href="https://www.forbes.com/sites/mnunez/2019/10/25/facebook-launches-news-tab-with-dozens-of-us-publishers-to-combat-fake-news/#463492803cfe">Forbes</a> and <a href="https://nypost.com/2019/10/25/facebook-to-partner-with-major-media-companies-for-facebook-news/">New York Post</a>). As well, Facebook ultimately has the say in what publishers are shown on their news tab which means their could be some bias (in August 2019 there was an audit to see if Facebook is more left leaning, as reported by <a href="https://www.axios.com/facebook-conservative-bias-audit-results-1de997b4-7192-4546-a452-b90ded43a968.html">Axios</a> and <a href="https://www.nationalreview.com/news/facebook-releases-audit-of-anti-conservative-bias/">National Review</a>) or issues with what exactly is free speech (according to <a href="https://www.washingtonpost.com/opinions/facebooks-policy-helps-most-those-candidates-who-lie-most/2019/10/21/5cf17822-f432-11e9-8cf0-4cc99f74d127_story.html">Washington Post</a> and <a href="https://www.cato.org/blog/facebook-future-free-speech">Cato @ Liberty</a>). But it is unclear how this will be dealt with and I'm sure many people have many more questions.
		<br> <br>
		Only time will tell if Facebook's news tab will work or not. 
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->