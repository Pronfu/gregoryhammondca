<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Turn off adblock for the sites you love | Gregory Hammond </title>
  <meta name="description" content="When was the last time you turned off adblock? Turn it off to help pay for the companies staff.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Turn off adblock for the sites you love </h3>
        <p>
         When was the last time you saw ads on your computer? Many people (including me) run adblock addons for our browsers to ensure we don't see ads, sites load quicker and we can just get on with our day.
	     <br> <br>
         If you have an adblock turned on, on a website it means they aren't getting paid for their work and it's costing them money to run the server which runs their website. Yes I know many sites run ads which may do something you don't want it to. But that just means that you don't visit that website anymore. 
	     <br> <br>
         So I challenge you to turn off adblock for the next news website you are on, that way the company can pay for it's servers, the staff, their computers and everything else related to running a business. It doesn't matter if the site takes a second or two more to load, it means you're paying them for their hard work. If you can't do that then just stop reading news site since they can't live forever.
         <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->