<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>I turned off email notifications, and it has increased my productivity | Gregory Hammond </title>
  <meta name="description" content="Email notifications take away from your productivity, why haven't you turned them off yet.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> I turned off email notifications, and it has increased my productivity </h3>
        <p>
        You might not notice it, every time you check something different other than what you are working on, it takes about 15 minutes for you to get focused to get back to the task at hand. If you get a notification for every email then you be wasting away hours trying to focus then getting distracted again.
        <br> <br>
        I have turned off all email notifications since nothing is an emergency and can wait. I still have my email program running,and checking for emails, and I can reminders from my calendar (I know I should use a separate program for my calendar, I haven't found anything that works for me yet). I now only check emails when I want (or I'm waiting for a reply), if it's an emergency then there are other ways to talk.
        <br> <br>
        Stop getting distracted by small things like email notifications and get back to your task at hand.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->