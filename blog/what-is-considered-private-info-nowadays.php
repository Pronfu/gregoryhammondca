<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>What is considered private information nowadays? | Gregory Hammond </title>
  <meta name="description" content="Over time we all have published more personal information about ourselves. But at what cost, and why do we need to be acknowledged by people we don't really know.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> What is considered private information nowadays? </h3>
        <p>
        <i> Date published: October 16th 2019 </i>
        <br> <br>
        Most people who use social media post many things (including their birthday, marriage, new job and more) but that has changed over time. It used to be that you kept that information private and only those close to you (family and close friends) knew about those details, but now it has changed to that if it isn't posted online (and someone gets a reminder about it) then it doesn't exist.
		<br> <br>
		<a href="http://glennf.com/">Glenn Fleishman</a> decided to change his Facebook settings to not tell people when his birthday was (he set it to private) and he didn't get any birthday wishes. Instead he heard from family, a couple of friends and in one Slack team that he is part of (someone in the group had made a note of everyone's birthday). I'll let you read the full details of his experiment at <a href="https://www.fastcompany.com/40550725/how-facebook-devalued-the-birthday">Fast Company</a>.
		<br> <br>
		We also update sites like LinkedIn to include our newest job, and many more things that people don't necessarily need to know about. So why do we do it? As Glenn says in the article <i>"While I liked the ego stroking of having a special day remembered by lots of people for a time, the artificiality of Facebook trying to raise a cheer for its own benefit on my behalf has worn thin."</i> So let's stop constantly updating and take time to look through everything we have posted and delete what won't matter in a week, month or even year. Just delete it without telling anyone, most people don't look back on older posts since people keep posting.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->