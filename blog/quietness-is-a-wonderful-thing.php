<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Quietness Is A Wonderful Thing | Gregory Hammond</title>
  <meta name="description" content="Have you ever been quiet? The world changes when everyone is quiet, we get more work done and we realize the wonderful world we live in.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Quietness Is A Wonderful Thing </h3>
        <p>
          Have you ever stopped and just became quiet? Like not been with people, not playing music, not reading. Just being quiet and listening? It sounds wonderful, we all need to take time and be quiet because we then have time to think and learn. 
	      <br> <br>
	      What if everyone in the world became quiet? Like no talking, no music, no social media. I think we would all get more stuff done. Have you tried not talking with anyone for a day? Tried not posting on social media for a day? It's hard but you get so much more done. Imagine if everyone stopped posting on social media for a day, we would all check once or twice during the day to see if anyone has posted but then we would all get back to work since nothing interesting is happening.
	      <br> <br>
	      If we all became quiet then the only thing we would hear is the wind, and maybe the clicking on a clock, and our own heartbeat. It sounds weird hearing your own heartbeat but it's a wonderful thing since you know you're still alive.
          <br>
          <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->