<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Abundance | Gregory Hammond </title>
  <meta name="description" content="We all have ample quanity, why aren't we giving more away.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Abundance </h3>
        <p>
        <i> Abundance is defined as (according to <a href="https://www.merriam-webster.com/dictionary/abundance">Merriam-Webster</a>) an ample quanity. </i>
	    <br> <br>
	    We all want an abundance of something, that may be time, it may be money or be may be love. But most of us already have an abundance of something, that may be pens, paper, or clothes.
	    <br> <br>
	    We all define abundance different, to one person an abundance could be one hundred of, to another it could be seven. But there are somethings we all already have an abundance of, houses, family, people on this earth, homeless people, the amount of food in our supermakets and many more things.
	    <br> <br>
	    Instead of wanting more, why don't we want less? It is the exact opposite of abundance, or if we already have an abundance why not give it away to those who have less than us. 
        </p>
        <br> <br>
        <?php include('blog-footer.php') ?>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->