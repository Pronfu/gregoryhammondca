<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why Apple Won't Do Research Into Phone Usage On Young People | Gregory Hammond </title>
  <meta name="description" content="Here is why Apple won't publicity do research on phone usage on young people and how it can affect their sales.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why Apple Won't Do Research Into Phone Usage On Young People </h3>
        <p>
        <i> Date published: January 7, 2018 </i>
        <br> <br>
         This post is in response to <a href="https://www.wsj.com/articles/iphones-and-children-are-a-toxic-pair-say-two-big-apple-investors-1515358834">iPhones and Children Are a Toxic Pair, Say Two Big Apple Investors</a> at Wall Street Journal (<a href="https://archive.fo/v3L9i">archive.today version for those who can't read the full version on WSJ</a>). If you haven't already please read article before reading this one.
	     <br> <br>
	     Apple will not do research into how phone usage affects young people (or children) as they will say that they aren't responsible for it as it's the parents responsibility to monitor how long their kids have been on the phone and to take it away when they feel they have spent too long on it. There are companies which have created apps that allow you to monitor how long someone has spent on a phone, so why should Apple do something that apps already can do.
	     <br> <br>
	     Apple will also say that they don't market their phones to kids, especially since most kids cannot afford the phone. So parents buy them then give them to their kid to keep them happy. That's not Apple's job to say you can't give your phone to someone who doesn't understand how long they should be on it for, Apple has done their due diligence.
	    <br> <br>
        You might think that because their are apps meant for kids then the phone must be meant for kids, but Apple didn't create those apps and while they do control what goes into their app store they don't know how some people will use their phones and they don't want to regulate it too hard.
        <br> <br>
        Apple will never publicity say that phones are bad and people should stop using them as that will decrease their sales (Apple has made $28.846bn from iPhone sales according to <a href="https://www.macworld.co.uk/news/apple/apple-financial-results-iphone-ipad-mac-sales-3581769/#toc-3581769-3">macworld</a>). The people who said that Apple should do a study impact might want to be careful also as less sales means their stocks are worth less and means they are losing money.
        <br> <br>
        <b> I understand the need to make sure phones don't go into the hands of really young kids but those stock holders need to also realize that Apple has made a ton of money from their phones and they don't want to give that money easily. </b>
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->