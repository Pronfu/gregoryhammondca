<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>3 lessons from being in business for 3 years | Gregory Hammond </title>
  <meta name="description" content="When you have your own business it can be tough. Here are 3 lessons / tips that I have learned through my years.">
  <link rel="canonical" href="https://www.linkedin.com/pulse/3-lessons-from-being-business-years-gregory-hammond" /> 

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> 3 lessons from being in business for 3 years </h3>
        <p>
        <i> Date published: January 6th 2020 </i>
        <br> <br>
        In January 2019 <a href="https://gjdev.ca/?ref=gregoryhammondcablog">Gregory J Development</a> will be in business for 3 years. Before I actually started this business I never thought I would have my own business. 3 years is 3 years, or 36 months, or 156 weeks, or 1,577,880 minutes. So here are 3 lessons I have learned from being in business for 3 years.
		<br> <br>
		<b>1) It's Not Easy</b>
		<br> <br>
		You see people like <a href="https://www.garyvaynerchuk.com/">Gary Vaynerchuk</a> who has everything that everyone wants. But you have to remember that it took 13 years working at <a href="https://winelibrary.com/">Wine Library</a> to know how to do some of the things he does today. He has also had <a href="https://vaynermedia.com/">Vayner Media</a> for 11 years. Gary is also a success story, we always have to remember that 1/3rd of businesses fail within the first two years. 
		<br> <br>
		There are times in a business that will be very busy, and times when nothing is happening. It's not easy.
		<br> <br>
		<b>1.5) Know Who You Can Have An Honest Business Conversation With</b>
		<br> <br>
		When you go in and start a business on your own it can get lonely and you may need someone to talk with about your business. You could consider them to be a mentor, or even a good friend. Be honest when talking with them, and they can help you through whatever problem you are having. 
		<br> <br>
		<b>2) Have A Niche</b>
		<br> <br>
		<i>Thanks to Aaron Sawyer over at <a href="https://kleurvision.com">Kleurvision</a> for this tip.</i>
		<br> <br>
		When you have a business you don't want to be the company that can do everything for everyone. Have a niche by doing what you are good at. Unsure about what you want to be your niche? What do you spend the majority of your business day doing? What do clients of yours pay you to do? That is want you to your niche to be, if you are still unsure talk to those closest to you and ask them.
		<br> <br>
		<b>3) Watch Where You Spend Money</b>
		<br> <br>
		When you have a business you have to learn how to balance income and expenses. Every business should also know when their year-end is. Expenses can be so different many things that it depends on the business. Every business must watch where their money goes to ensure they have more income at year-end.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->