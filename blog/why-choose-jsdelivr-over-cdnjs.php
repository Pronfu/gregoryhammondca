<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why Choose JsDelivr Over Cdnjs | Gregory Hammond </title>
  <meta name="description" content="JsDelivr and Cdnjs are close to the same thing. But this post will explain why you should choose JsDelivr.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why Choose JsDelivr Over Cdnjs </h3>
        <p>
        <i> Date published: September 2nd 2018 </i>
        <br> <br>
        The world changes so much, so quickly and while one thing may be blocked in one country it's widely used in another. When creating your website you never really know who will visit your website, that's why it's important to do what you can to make your things available around the world. 
		<br> <br>
		If you found this article via a search engine then you already know what a cdn is, if you don't know what a cdn is, it allows you have to your files available around the world. JsDelivr and Cdnjs allow you to have a cdn for anything open-source.
		<br> <br>
		So, for example if you wanted <a href="https://getbootstrap.com/">Bootstrap framework</a>, you could use <a href="https://www.bootstrapcdn.com/">their cdn</a> or use <a href="https://www.jsdelivr.com/package/npm/bootstrap">JsDelivr</a> and have it more readily available.
		<br> <br>
		Jsdelivr also works in China, China is a country where stuff is heavily censoured so if you don't want users in China to wait a long time for your site to load (or time-out all together) then use JsDelivr. In fact according to the <a href="https://www.jsdelivr.com/network#china">jsdelivr network page</a>, they are the only public CDN with a valid <a href="https://en.wikipedia.org/wiki/ICP_license">ICP license</a> issued by the Chinese government. This gives your company an edge over others. As of the time of writing make sure not to use Google fonts (or anything else by Google) if you want your site to be acccessible to those in China since China blocks anything Google related.
		<br> <br>
		JsDelivr doesn't just rely on one network, besides China, they rely on 4 different cdn providers to efficently run their service. They have a cool <a href="https://www.jsdelivr.com/network/infographic">infographic</a> if you visually want to see how it works. If one thing goes down jsdelivr can just keep up, and not have to worry.
		<br> <br>
		You can manually add any project / package from npm, github, or even WordPress.org without having to wait for approval. 
		<br> <br>
		<picture>
		 <img src="https://res.cloudinary.com/doklvodcx/image/upload/f_auto/v1534710031/izlthk_wjd25q.png">
		</picture>
		<br>
		<picture>
		 <img src="https://res.cloudinary.com/doklvodcx/image/upload/f_auto/v1534710105/hnejia_xofxkm.png">
		</picture>
		<br>
		<picture>
		 <img src="https://res.cloudinary.com/doklvodcx/image/upload/f_auto/v1534710162/dvbyhu_wjec0o.png">
		</picture>
		<br> <br>
		If you use npm packages then you how important it is to ensure they are updated, jsdelivr automatically gets the latest version and if a version gets removed jsdelivr will keep the version so your website won't break. 
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->