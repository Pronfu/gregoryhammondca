<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why I'm Using PGP (Pretty Good Privacy) Again | Gregory Hammond </title>
  <meta name="description" content="I used PGP then stopped but now I'm using it again and I believe everyone should use PGP.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why I'm Using PGP (Pretty Good Privacy) Again</h3>
        <p>
        <i> Date published: August 4th 2018 </i>
        <br> <br>
        Back in 2014 I created PGP keys and did the usual thing of putting my public key on <a href="https://en.wikipedia.org/wiki/Key_server_(cryptographic)">key servers</a>, and even created a <a href="https://en.wikipedia.org/wiki/Keybase">Keybase</a> account. But I only had one little problem, nobody I knew I had or used a PGP key.
        <br> <br>
        In late 2017 I just really stopped using it, and especially after <a href="https://en.wikipedia.org/wiki/EFAIL">EFAIL</a> I was put off my using PGP. But yesterday (August 3rd) I wanted to start using PGP again, so I made sure I had my PGP keys working, and I uploaded one key to key servers, and now I have a <a href="../pgp">PGP page on this site</a> so you know all my public pgp keys.
        <br> <br>
        You are probably wondering where my Keybase account is, I had deleted my previous one and I don't plan on setting it up again. Why you we need a single time to prove who someone is (with their pgp key). What if any of the social platforms where you post that proof get hacked? On every social media platform (haven't thought of the right way for LinkedIn) where I use my name I'm going to have a pinned post that says where people can find my pgp keys.
        <br> <br>
        PGP is easy to use, and with guides from companies like <a href="https://ssd.eff.org/en/module/how-use-pgp-windows">EFF</a> there is almost no excuse. 
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->