<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why No WordPress | Gregory Hammond </title>
  <meta name="description" content="I design & develop WordPress sites for my business, but why didn't I do a WordPress site for my own site?">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why No WordPress </h3>
        <p>
        <i> Date published: July 23rd 2018 (2018-07-23) </i>
        <br> <br>
        You are probably wondering why I didn't do a WordPress site, since my business (<a href="https://gjdev.ca?ref=gregoryhammondsite">Gregory J Development</a>) mostly does WordPress sites (as well as custom sites). There are some simple points as to why.
        <br> <br>
        WordPress requires constant updates: In WordPress there are always updates, either theme updates, plugin updates or even WordPress itself needs an update. I didn't want to constantly go into my site to update it.
        <br> <br>
        Static sites can't really be hacked: There are constant attacks on WordPress, so why put myself throught the stress and pain when I could just do a static site and not have to worry (I know I could use a static site plugin but refer to the first point).
        <br> <br>
        WordPress is bloated: WordPress sites have a big page size, and has so many things that aren't used by everyone. I wanted my site to be small, lightweight and accessible. As an example, to get a "skip to content" link at the top of every page requires a plugin, which could really increase the size of each page.
        <br> <br>
        I wanted to be able to change my site as I please: With WordPress you are stuck with what is put out, when you create your own site you can customize it however you want.
        <br> <br>
        Wanted to learn CSS Grid: I have heard about CSS grid for a while now, and I just wanted to learn it and use it.
        <br> <br>
        WordPress felt like a cop-out: There are tons of sites with WordPress and it just felt like I would be following in their foot-steps. Why not try and learn something new?
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->