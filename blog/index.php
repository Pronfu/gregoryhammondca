<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Blog | Gregory Hammond </title>
  <meta name="description" content="A list of all blog posts by Gregory Hammond.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Blog </h3>
        <p>
         Here is all the blog posts that I have written for this website. These are my own thoughts and experiences and do not represent in anyway my past/current/future employers (or clients).
		 <br> <br>
		 <a href="./easy-ways-dataprivacyday">Some Easy Ways To Protect Your Data & Online Privacy (2020-01-28)</a>
		 <br> <br>
		 <a href="./brands-ripeanut">Why Brands Latched Onto #RIPeanut (2020-01-23)</a>
		 <br> <br>
		 <a href="./3-lessons-being-in-business-3-years">3 lessons from being in business for 3 years (2020-01-06))</a>
		 <br> <br>
		 <a href="./facebook-news-tab-may-or-may-not-work">Facebook News Tab: Why It May Or May Not Work  (2019-10-26))</a>
		 <br> <br>
		 <a href="./what-is-considered-private-info-nowadays">What is considered private information nowadays?  (2019-10-16))</a>
		 <br> <br>
		 <a href="./why-you-shouldnt-self-host-all">Why You Shouldn't Self-Host Your Assets  (2019-06-04))</a>
		 <br> <br>
		 <a href="./the-investors-coming-back">The Investors Will Soon Be Coming Back (2019-05-27))</a>
		 <br> <br>
		 <a href="./good-job-jeff-bezos-feb-2019">Good Job Jeff Bezos for Standing Up Against Extortion (2019-02-08))</a>
		 <br> <br>
		 <a href="./why-i-support-takeaknee-and-no-support-nfl">Why I Support #TakeAKnee and Will Never Support the NFL (2019-02-03))</a>
		 <br> <br>
		 <a href="./no-goals-2019">No Goals For 2019 (2018-12-29))</a>
		 <br> <br>
		 <a href="./attention-to-apple-events">Why I Don't Pay Attention To Apple Events Anymore (2018-10-26))</a>
         <br> <br>
		 <a href="./devhub-toronto-review">Devhub Toronto Review (2018-10-07))</a>
         <br> <br>
		 <a href="./why-i-dont-have-a-uses-page">Why I Don't Have A Uses Page (2018-09-05))</a>
		 <br> <br>
		 <a href="./why-choose-jsdelivr-over-cdnjs">Why Choose JsDelivr Over Cdnjs (2018-09-02))</a>
		 <br> <br>
		 <a href="./no-more-sorry-for-the-delayed-response">Time to stop saying "Sorry for the delayed response" (2018-08-13)</a>
		 <br> <br>
         <a href="./why-i-use-pgp-again">Why I'm Using PGP (Pretty Good Privacy) Again (2018-08-04)</a>
         <br> <br>
         <a href="./why-no-wordpress">Why No WordPress (2018-07-23)</a>
         <br> <br>
         <a href="./grid-template-columns-responsive">How to get grid-template-columns to be responsive (2018-07-23)</a>
         <br> <br>
         <a href="./why-apple-wont-do-research-into-phone-usage-on-young-people">Why Apple Won't Do Research Into Phone Usage On Young People</a>
         <br> <br>
		 <a href="./why-i-dont-like-microsoft-has-acquired-github">Why I Don't Like That Microsoft Has Acquired GitHub</a>
		 <br> <br>
	     <a href="./abundance">Abundance</a>
	     <br> <br>
	     <a href="./why-i-dont-provide-my-phone-number-in-email-signature">Why I Don't Provide My Phone Number In My Email Signature</a>
	     <br> <br>
	     <a href="./security-vs-convenience">Security vs Convience</a>
	     <br> <br>
	     <a href="./protests-work-violence-doesnt">Protests Work, Violence Doesn't</a>
         <br> <br>
         <a href="./why-i-volunteer">Why I Volunteer - Think Long Term</a>
         <br> <br>
	     <a href="./quietness-is-a-wonderful-thing">Quietness Is A Wonderful Thing</a>
	     <br> <br>
	     <a href="./why-im-a-night-owl">Why I'm A Night Owl, Not A Early Riser</a>
	     <br> <br>
	     <a href="./turn-off-adblock">Turn off adblock for the sites you love</a>
	     <br> <br>
	     <a href="./tricked-into-bank-payment-protector">Don't get tricked into your bank's payment protector / credit card insurance</a>
	     <br> <br>
         <a href="./turn-off-email-notifications">I turned off email notifications, and it has increased my productivity</a>
         <br> <br>
         <a href="./too-many-methods-of-communication">Too Many Methods Of Communication</a>
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->