<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why You Shouldn't Self-Host Your Assets | Gregory Hammond </title>
  <meta name="description" content="Self-hosting everything on your website isn't the best thing to do, you could also use a CDN but that may not be the best solution.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why You Shouldn't Self-Host Your Assets </h3>
        <p>
        <i> Date published: June 4th 2019 </i>
        <br> <br>
        Harry Roberts put out an article out why you should <a href="https://csswizardry.com/2019/05/self-host-your-static-assets/">self-host your static assets</a>, I agree with some of his points but I also disagree with some of them. Please read his article before you read this.
		<br> <br>
		When you use a CDN (such as <a href="http://code.jquery.com/">code.jquery.com</a> or <a href="https://www.jsdelivr.com/">jsdelivr.com</a>) it takes whatever you are pointing to and moves it off your own server and let's another company handle it who has more servers (around the world) and more expertise.
		<br> <br>
		<h5>Use the right CDN provider</h5>
		<br> <br>
		While there may be some slowdowns if you are a CDN, you may not have the right CDN provider. There are so many CDN providers you just have to find the one that fits both you and your customers. There is always outages, no matter if you self-host it or put it on a CDN, nothing is 100% online. 
		<br> <br>
		Services can always be shut down (and without notice) but that is why you should use a CDN provider that has a good reputation, and have a backup so you can easily switch CDN providers if you need to.
		<br> <br>
		By using a CDN your website may be available in parts of the world that it wasn't previously. Even if it's some text, it's better than your site as a whole not being available.
		<br> <br>
		If you don't use a CDN behind your own website then it means that every time someone visits your website the computer have to go to where your server is located (which could be on the other side of the world) and get the content then come back to display it for you. But if you use a CDN then your content will most likely view faster since the visitor will go the nearest CDN network spot and get the content from there. It isn't always faster as sometimes some closest network spots can be very far away. So you want to choose a CDN provider that have tons of spots close to where your target audience (and readers) will come from. 
		<br> <br> <br>
		<h5>Security Vulnerabilities - It will happen to anyone</h5>
		<br> <br>
		There will always be security vulnerabilities, even if you self-host them you could have a vulnerability. If you don't want someone to know about it then it shouldn't be online. SRI (<a href="https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity">subresource integrity</a>) then help to ensure that something isn't changed. While about <a href="https://caniuse.com/#feat=subresource-integrity">90% of all browsers</a> do accept and use SRI there are still browsers that don't accept SRI so you can't be 100% sure that everyone will use SRI.
		<br> <br> <br>
		<h5>Use multiple CDN providers - to ensure downtime is kept to a minimum</h5>
		<br> <br>
		You should use multiple CDN's, if something goes down in one company then your entire website doesn't go down. You may think that it means you will have to pay more, but there are plenty of free CDN providers. Most of the time free means that you are the product but that isn't always the case. Some free CDN's include: <a href="https://www.cloudflare.com/">Cloudflare</a>, <a href="https://aws.amazon.com/cloudfront/">Amazon AWS</a> has a free tier, <a href="https://cloudinary.com/invites/lpov9zyyucivvxsnalc5/atgngbnswtfuaadqe4eb" rel="nofollow">Cloudinary</a> (referral link), <a href="https://www.jsdelivr.com/">jsDelivr</a>, and <a href="https://www.gitcdn.xyz/">GitCDN</a> are just some free CDN providers.
		<br> <br> <br>
		<h5>Using a CDN means faster for all websites</h5>
		<br> <br>
		Sometimes it seems like a website will take longer to load when using a CDN, it won't always. If you point to the same version of jquery (and same CDN provider) that another website is using then it's faster for the user. It may not be faster for you, but we want to speed up the web and by doing a little bit we can speed up everyone's visiting experience.
		<br> <br> <br>
		<h5>Caching - It's a good thing</h5>
		<br> <br>
		Sometimes caching is a good thing, it may take longer on the first time for your website to load and it may look like your website never changes. But it means that when someone visits your website again they can more quickly load the website and if you set the cache of the actual content low enough then the visitor can easily see the new content. You can set the cache to be higher for your CSS or something that will rarely changes to quickly load it.
		<br> <br> <br>
		<h5>A CDN may not be the solution - but self-hosting isn't the best thing</h5>
		<br> <br>
		Yes there are times when you should self-host but overall using a CDN to speed up your own site and the entire web in general (especially if multiple websites use the same CDN and point to exactly the same version). Using a CDN is not just about website performance it's about speeding up the entire web. There is a fine balance between self-hosting everything and using a CDN, it's probably better to use a mix of both and this all depends on your own needs.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->