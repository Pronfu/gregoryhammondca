<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title> Protests Work, Violence Doesn't | Gregory Hammond </title>
  <meta name="description" content="Time and time again protests have worked, while violence just shows that you have money and are willing to die for what you believe in.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Protests Work, Violence Doesn't </h3>
        <p>
         You may have heard about the protests happening in Charlottesville, basically the city of Charlottesville (in central Virginia, in the USA) has had a planned removal of a Confederate monument and of course people are very mad that they want to do that. There are many news companies putting out articles about it, <a href="http://www.aljazeera.com/indepth/features/2017/08/alt-rally-charlottesville-braces-violence-170810073156023.html">AlJazerra</a>, <a href="http://www.nbcnews.com/news/us-news/torch-wielding-white-supremacists-march-university-virginia-n792021">NBCNews</a>, <a href="http://variety.com/2017/biz/news/white-nationalist-protest-charlottesville-racism-black-lives-matter-1202525715/">Variety</a>, <a href="http://www.bbc.com/news/world-us-canada-40909547">BBC</a>, <a href="http://www.cbc.ca/news/world/charlottesville-virginia-white-nationalists-march-1.4245131">CBC</a>.
	    <br> <br>
	    Everyone has the right to protest for something they believe in, and protests have worked in the past. <a href="https://www.theguardian.com/commentisfree/2017/apr/10/protesting-cnd-iraq-opinion">The Guardian</a>, <a href="https://www.vice.com/en_us/article/aeabb8/why-protests-work">Vice</a> and <a href="http://www.huffingtonpost.com/johann-hari/protest-works-just-look-a_b_781339.html">Huffington Post</a> are just some websites that have covered that protests do work. What is wrong, inhumane and just stupid is becoming violent, violence just shows that you have money and are willing to fight (and sometimes die) for something you believe in. 
	    <br><br>
	   If you don't want something to happen then have a petition, do a protest, have a march, and see if the higher-ups will listen, but you can't force anyone to listen to your opinion.
     <br>
     <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->