<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>No Goals For 2019 | Gregory Hammond </title>
  <meta name="description" content="A new year means resolutions, but I don't have any goals or resolutions for 2019.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> No Goals For 2019 </h3>
        <p>
        <i> Date published: December 29th 2018 </i>
        <br> <br>
        It's the end of 2018 and everyone talks about what new years resolutions they are going to make. What many people seem to forget is that most of those resolutions never happen for various reasons. I don't have any goals (or resolutions you can call them) for 2019.
		<br> <br>
		I care about growing my <a href="https://gjdev.ca/?ref=gregorynogoaldsblogpost">business</a>, spending time with friends and family and enjoying my life. It may seem weird but you never know how your life can change.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->