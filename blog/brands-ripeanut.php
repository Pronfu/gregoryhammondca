<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why Brands Latched Onto #RIPeanut | Gregory Hammond </title>
  <meta name="description" content="#RIPeanut has been very big and many big brands have latched onto it, but why when it's just a mascot.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why Brands Latched Onto #RIPeanut </h3>
        <p>
        <i> Date published: January 23rd, 2020 </i>
		<br>
		<i> Date updated: Febuary 3rd, 2020 </i> Update can be found at bottom of article.
        <br> <br>
        Planters (owned by <a href="https://en.wikipedia.org/wiki/Kraft_Heinz#Brands">Kraft Heinz</a>) decided to kill off their well known mascot (Mr. Peanut) in both a <a href="https://www.businesswire.com/news/home/20200122005567/en/MR.-PEANUT-Passes-104-Years-Sacrificing-Save">press</a> <a href="https://news.kraftheinzcompany.com/press-release/brand/mr-peanut-passes-away-104-years-old-sacrificing-himself-save-friends-matt-walsh-">release</a> and a <a href="https://www.youtube.com/watch?v=6UIKq9u6xUM">YouTube video</a> and brands have latched onto this in a big way.
		<br> <br>
		When Planters decided to announce this <a href="https://twitter.com/MrPeanut/status/1220013408180895745">on social media</a> there were are so many replies to the tweet and many people have jumped on <a href="https://twitter.com/hashtag/RIPeanut?src=hash">#RIPeanut</a>. Many big brands have jumped on the hashtag (or the initial tweet) from <a href="https://twitter.com/SNICKERS/status/1220030449164201984">Snickers</a> to <a href="https://twitter.com/RealMrClean/status/1220013258473725956">Mr. Clean</a> to <a href="https://twitter.com/Oreo/status/1220039499863986177">Oreo</a> and even <a href="https://twitter.com/reeses/status/1220118516247990278">Reese's</a>.
		<br> <br>
		Why have brands latched on? There could be many reasons, from the companies knowing ahead of time (and having a response ready), to seeing it in the news and quickly making up something, to the company being owned by Kraft Heinz and knowing that this was going to happen since Kraft Heinz <a href="https://en.wikipedia.org/wiki/Kraft_Heinz#Brands">owns many companies</a>.
		<br> <br>
		Unfortunately no company has publicly say why they have launched onto this, so we will just have to wait and see what happens with Mr. Peanut when "his funeral" <a href="https://adage.com/article/special-report-super-bowl/planters-really-has-killed-mr-peanut-funeral-planned-super-bowl/2229566">airs during the third quarter of the Super Bowl LIV</a>.
		<br> <br>
		<b>UPDATE:</b> On Febuary 2nd 2020 Planters <a href="https://www.youtube.com/watch?v=MoVpgtAJHfU">released their commerical to YouTube</a> (which currently <a href="https://unblockvideos.com/youtube-video-restriction-checker/#url=MoVpgtAJHfU">isn't blocked anywhere in the world</a>) which basically shows "the funeral" but the <a href="https://en.wikipedia.org/wiki/Kool-Aid_Man">Kool-aid man</a> cries a tear and out comes a baby version of Mr.Peanut who says "I'm back". The brand probably did this because the sales have been flat, and they wanted to change their brand mascot (which <a href="https://www.qualitylogoproducts.com/blog/brand-mascots-change-not-always-bad/">isn't always a bad thing when done right</a>) to reflect current consumers, and that baby mascots are becoming popular as seen by <a href="https://trends.google.com/trends/explore/TIMESERIES/1580780400?hl=en-US&tz=300&date=2019-11-03+2020-02-03&geo=US&q=%2Fm%2F02mwjq,baby+yoda,%23babynut&sni=3">this chart</a> (which is also shown below)
		<br> <br>
		<script type="text/javascript" src="https://ssl.gstatic.com/trends_nrtr/2051_RC11/embed_loader.js"></script> <script type="text/javascript"> trends.embed.renderExploreWidget("TIMESERIES", {"comparisonItem":[{"keyword":"/m/02mwjq","geo":"US","time":"2019-11-03 2020-02-03"},{"keyword":"baby yoda","geo":"US","time":"2019-11-03 2020-02-03"},{"keyword":"#babynut","geo":"US","time":"2019-11-03 2020-02-03"}],"category":0,"property":""}, {"exploreQuery":"date=2019-11-03%202020-02-03&geo=US&q=%2Fm%2F02mwjq,baby%20yoda,%23babynut","guestPath":"https://trends.google.com:443/trends/embed/"}); </script> 
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->