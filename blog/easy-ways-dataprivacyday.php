<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Some Easy Ways To Protect Your Data & Online Privacy | Gregory Hammond </title>
  <meta name="description" content="January 28th of every year is #DataPrivacyDay, it's time to protect your own data and online privacy.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Some Easy Ways To Protect Your Data & Online Privacy </h3>
        <p>
        <i> Date published: January 28th 2020 </i>
        <br> <br>
        <a href="https://staysafeonline.org/data-privacy-day/">#DataPrivacyDay</a> is January 28th of each year, and it's meant to remind people about their online privacy and ways to protect their own data while online. I thought it would be good to go over some easy ways to protect your own data and start caring about your online privacy.
		<br> <br>
		<b>Keeping Your Computer Updated</b>
		<br> <br>
		Every update comes with patches and bug fixes, so make sure to keep your operating system and all your programs updated. How often you do this is up to you.
		<br> <br>
		<b>Using Browser Addons</b>
		<br> <br>
		Most browsers like <a href="https://www.mozilla.org/en-US/firefox/">Firefox</a>, <a href="https://www.google.com/chrome/">Chrome</a> and <a href="https://www.apple.com/safari/">Safari</a> allow you to install addons which are things mostly created by volunteers to enhance your browser. There are many addons that can protect your data & online privacy. Some of the more popular and well known ones are <a href="https://github.com/gorhill/uBlock/#installation">uBlock Origin</a> (not to be confused with <a href="https://en.wikipedia.org/wiki/UBlock_Origin#uBlock">uBlock</a>), <a href="https://www.eff.org/https-everywhere">HTTPS Everywhere</a>, and <a href="https://www.eff.org/privacybadger">Privacy Badger</a> that help with your online privacy. Even the default settings for these addons is better than nothing.
		<br> <br>
		<b>Stop Using Your Own Name And Picture Online</b>
		<br> <br>
		Anywhere an account is needed today people seem to use their own name, and their own picture. If you want to protect your data and online privacy don't do this. You can just use a username (try to use a different username on every site so that people don't find out your username) and use a <a href="https://unsplash.com/">stock photo</a>.
		<br> <br>
		<b>Consider Using A Paid VPN When Out In Public</b>
		<br> <br>
		When you are out in public connecting to a wifi you don't own you don't know who else is on the network and what they can capture about you. If you use a paid VPN then you don't have to worry about someone capturing your data and either selling it or doing who knows what with it. There is no best vpn, the best vpn is the one that you trust the most. <a href="https://thatoneprivacysite.net/">That One Privacy Guy</a> has put together a <a href="https://thatoneprivacysite.net/vpn-comparison-chart/">chart that details well known VPN's and what information they collect and where they are located so you should start there</a>.
		<br> <br>
		<b>What Do You Consider To Be Private Information</b>
		<br> <br>
		Everyone has their own version of privacy, and what they consider to be private information. Take some time to think about you consider to be private information, and <a href="https://gregoryhammond.ca/blog/what-is-considered-private-info-nowadays">read more on another of my blog posts</a>.
		<br> <br>
		<b>Stop Using Free Things</b>
		<br> <br>
		If you want to take it to the next level then consider that service that is free has to make money somehow, they are most likely making money by selling your data. If it's free then you are the product. Sometimes free things are also paid for by someone else, so check every sites privacy policy.
		<br> <br>
		There are many more ways to protect your data and online privacy, and I would suggest if you want to learn more check out <a href="https://old.reddit.com/r/privacy/">/r/privacy</a> or <a href="https://www.privacytools.io/">privacytools.io</a>. 
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->