<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why I Don't Pay Attention To Apple Events Anymore | Gregory Hammond </title>
  <meta name="description" content="Apple events are so often now that they should just be one email.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why I Don't Pay Attention To Apple Events Anymore </h3>
        <p>
        <i> Date published: October 26, 2018 </i>
        <br> <br>
        It seems like every couple of months there are a new rumor about Apple announcing a new product, or a picture of a device in-progress, or sometimes that Apple has that is new. Since these rumors happen so often, and there seems to be an Apple event every 4 months I have stopped paying attention to these Apple rumor and events.
		<br> <br>
		Apple keeps announcing new things to ensure they stay on top, in terms of profit (<a href="https://www.theguardian.com/technology/2018/aug/02/apple-becomes-worlds-first-trillion-dollar-company">they became the first company to be a trillion-dollar company</a>), <a href="https://en.wikipedia.org/wiki/Top-of-mind_awareness">top of mind awareness</a>, and staying relevant (<a href="https://www.fastcompany.com/3020297/6-ways-apple-can-stay-relevant">FastCompany</a>, <a href="https://www.imore.com/how-apple-will-keep-iphone-se-relevant">iMore</a>). But what they announce could just be sent out in an email.
		<br> <br>
		Apple should have a press email address and collect email addresses from all the companies that want to be kept updated on new Apple things. Then send an email out about the new items and include links to it on their website. The main reason why Apple doesn't do this is, to be able to limit / have certain people in their event, and to do demos. 
		<br> <br>
		If Apple just sent one email it would save so many people (including Apple employees) so much time, it would allow everyone to have the information at the same time (then there could be quick stories and detailed stories) and it would save money (companies don't have to fly certain people out, they don't have to buy 3G and use it, then write up what was just said). 
		<br> <br>
		Apple please just put out one email for each event.
		<br> <br>
		<a href="https://twitter.com/louisvirtel/status/1039924466506129408"> <img src="https://res.cloudinary.com/doklvodcx/image/upload/f_auto,q_auto/v1540607054/8oru7W_z4il5y.png" alt="Tweet that says: Apple Events are the world's largest meeting that could've been an email."> </a>
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->