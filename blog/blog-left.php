        Welcome to the blog of Gregory Hammond.
        <br> <br>
        <a href="../">Home Page</a>
        <br> <br>
        <a href="../support">Support The Site</a>
        <br> <br>
        <a href="javascript:history.back()">Go To Previous Page</a>
        <br> <br>
        <a href="feed.xml">Blog RSS Feed</a>
        
       <br> <br> <br>
       Data jurisdiction statement(<a href="http://werd.io/entry/5213969abed7de1978b43b2d/government-the-last-great-gatekeeper-is-ripe-for-disruption">what is this?</a>):
       <br>
       I'm currently living in Canada, my website is hosted by on a vps (located in Toronto, Canada) I pay for by <a href="https://www.cloudways.com/en/?id=338165">Cloudways</a> (affiliate link), on a server owned by DigitalOcean (which is <a href="https://web.archive.org/web/20170114040700/http://www.data-jurisdiction.org/country/US">US</a> based). Cloudflare is protecting this site (which is <a href="https://web.archive.org/web/20170114040700/http://www.data-jurisdiction.org/country/US">US</a> based). Email is hosted by <a href="https://runbox.com">Runbox</a> in Norway. I backup everything to Backblaze (<a href="https://web.archive.org/web/20170114040700/http://www.data-jurisdiction.org/country/US">US</a> as well). If you don't wish for our conversation to be logged (or backed up to Backblaze) email me asking for my XMPP (I have OTR).