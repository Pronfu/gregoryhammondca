<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why I Support #TakeAKnee and Will Never Support the NFL | Gregory Hammond </title>
  <meta name="description" content="#TakeAKnee is so important and something that the NFL doesn't support, so I won't support them.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why I Support #TakeAKnee and Will Never Support the NFL </h3>
        <p>
        <i> Date published: Febuary 3rd 2019 </i>
        <br> <br>
        In the summer of 2016 Colin Kaepernick told a huge step that changed his career. He decided not to stand during the US national anthem. At first he just told a seat, then the next week he told a knee. He did this to protest what is happening to African Americans (and minorities) in the US. "I am not going to stand up to show pride in a flag for a country that oppresses black people and people of color," he said in an <a href="http://www.nfl.com/news/story/0ap3000000691077/article/colin-kaepernick-explains-why-he-sat-during-national-anthem">interview after that game</a>. At that time his coach said (to reporters) "Kaepernick's decision not to stand during the national anthem is his right as a citizen" and said "it's not my right to tell him not to do something.". But that quickly changed.
		<br> <br>
		In September 2017 the then US President (Donald Trump) said "owners should fire players who kneel during the national anthem. And I'm encouraging spectators to walk out in protest." (Source: <a href="https://www.latimes.com/nation/nationnow/la-na-trump-nfl-anthem-20170922-story.html">LaTimes</a>). In May 2018 the NFL made a new policy that all players on the field must stand for the national anthem (or they can remain in the locker room without penalty), by announcing this they have "chosen to bury their heads and silence players." (the NAACP criticized this policy and said this). (Source: <a href="https://www.nbcnews.com/news/us-news/nfl-announces-new-national-anthem-policy-fines-teams-if-players-n876816">NBCNews</a>)
		<br> <br>
		Everyone has their own rights and thoughts, as the NFL is a private company they <a href="https://www.vox.com/culture/2017/9/25/16360580/nfl-donald-trump-national-anthem-protest">can do almost whatever they want</a>, but that doesn't mean it's not right and won't get supporters and haters.  
		<br> <br>
		I support the NFL for making a policy so it is clear, but I don't support them because they are telling players that they don't care about their views and what goes on in the world outside of football. It would have been interesting if the NFL said they support players to <a href="https://twitter.com/hashtag/takeaknee/">#TakeAKnee</a> and if Donald Trump had still said to fire players, Donald Trump probably would have changed it to say bankrupt the NFL (which as a reminder is a private company and the only way they can go bankrupt is if the sponsors & supporters leave).
		<br> <br>
		Until the NFL reverses their decision, signs Colin Kaepernick to a team and makes a public statement (that they were wrong, they will pay Colin his backpay, are going to donate a huge amount of money to charities which help African Americans and minorities) I won't be watching or supporting the NFL in any way that I can.
		<br> <br>
		<b>Agree? Disagree? Write your own post somewhere so others can know your opinion. </b>
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->