<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Time to stop saying "Sorry for the delayed response" | Gregory Hammond </title>
  <meta name="description" content="Sorry for the delayed response is something we say too often. Time to stop and realise that you shouldn't always be on email.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Time to stop saying "Sorry for the delayed response" </h3>
        <p>
        <i> Date published: August 13th 2018 </i>
        <br> <br>
        Most of the time if people take more than a day to respond to an email they will start their reply with "Sorry for the delayed response" and while it's nice, it's also something we need to stop doing.
		<br> <br>
		We shouldn't be tied to our email and made to respond to email quickly, I love this tweet by Jason Fried.
		<br>
		<picture>
         <a href="https://twitter.com/jasonfried/status/1002567596883841024"> <img src="https://res.cloudinary.com/doklvodcx/image/upload/f_auto/v1534614943/0928345a-16eb-42c7-a153-92834e579617_mmvfgk.png" alt="Don't apologize for taking a couple of days to get back to someone"> </a>
        </picture>
		<br> <br>
		We have lives outside of our email, some people even get notifications on their phone for every email they get. You don't need to. 
		<br> <br>
		<picture>
         <a href="https://twitter.com/jasonfried/status/1002983183716093958"> <img src="https://res.cloudinary.com/doklvodcx/image/upload/f_auto/v1534614943/b92f914e-f986-439f-8bef-a33ac83bf92c_uwihtg.png" alt="I'd wager the majority of emails I sent were sent too fast."> </a>
        </picture>
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->