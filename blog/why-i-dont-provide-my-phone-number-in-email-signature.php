<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why I Don't Provide My Phone Number In My Email Signature | Gregory Hammond </title>
  <meta name="description" content="A phone number is something that almost everyone has, but why should it be public on every website and email signature?">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why I Don't Provide My Phone Number In My Email Signature </h3>
        <p>
          Email is something almost everyone uses in some form or another, and you need an email signature otherwise it looks like you don't know how to use email. But everyone has a different email signature, and some people think certain things are needed and some people think otherwise.
	      <br> <br>
	      On my email signature there is my name, title, and website. That is all. I don't need to include an office (yet), or a phone number or a quote. If you have an office (that isn't your house) then put your office location so people can visit you, if you have a phone in that office then put that phone number. I have a phone that I use for business, but I email so many people and they don't all need my phone number.
	      <br> <br>
	      If you need my phone number then either email me and ask me to call you (I have done it before and I'm happy to do it), or ask for my phone number (I'm happy to provide it if you want to talk but I don't want everyone to have it). I will probably change my mind when I get older but for right now if you want to call me and don't have my number then simply ask.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->