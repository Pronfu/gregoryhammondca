<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why I Don't Have A Uses Page | Gregory Hammond </title>
  <meta name="description" content="Many developers are putting out uses pages, there are reasons why you shouldn't.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why I Don't Have A Uses Page </h3>
        <p>
        <i>Date published: September 5th 2018 </i>
        <br> <br>
        Many developers are putting out their own uses page, to write out what they use if others are interested.
		<br> <br>
		I will never have a uses page on any of my websites.
		<br> <br>
		The main reason is because of <a href="https://en.wikipedia.org/wiki/Open-source_intelligence">OSINT (open-source intelligence)</a> and if people know you are using a certain piece of software then they can find a <a href="https://en.wikipedia.org/wiki/Zero-day_(computing)">zero-day</a> and exploit the software, then your computer and who knows what else.
		<br> <br>
		Think about what you post online, and how it may be used as intelligence against you.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->