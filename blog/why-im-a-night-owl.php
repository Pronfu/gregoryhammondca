<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why I'm A Night Owl, Not A Early Riser | Gregory Hammond </title>
  <meta name="description" content="Is an early riser someone you should be proud of? Shouldn't you also be proud of an night owl?">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why I'm A Night Owl, Not A Early Riser </h3>
        <p>
          Many people are proud that they wake up early (before 5am), but you never hear from those who work from 10pm to 2am. These people also known as night owl's.
	      <br> <br>
	      Everyone is different, our bodies sometimes get out of rhythm and wake up early or late, like I did for 2 days in a row (my body woke me up at 6:30am). If you don't know if you are a early riser, a normal person, or a late owl then think about what time you usually go to bed. If you go to bed between 8 to 10pm then you are an early riser. If you go to bed between 11pm - 1am then you are a normal person, any later then you are a night owl.
	      <br> <br>
	      I'm a night owl most days, I often go to bed around 2am and get up around 10am now that doesn't mean I can't get up any earlier, or go to bed earlier. After 7pm most people have stopped working and spend their reminding time with family. I spend time with my family during that time but after I'm back to work. Please don't email me at 11pm and expect a response within 20 minutes because I will respond to you the next day.
	      <br> <br>
	      We all have those times when we "work late" or get up in the middle of the night remembering how to do something then we have trouble getting back to sleep. I do the same thing, not very often since I have learned from my mistakes. Remember things can always wait until the next day, unless all tickets are sold out and then need them off the website to ensure people don't complain.
	      <br> <br>
	      Remember everyone has their own way of life, don't compare my life to yours and say you should be doing what I'm doing because your body may not like it. I have figured out the way my body works, and tried to work with it rather than against it. You may be a normal person which is a good thing because that means you work normal hours (9 to 5). 
	      <br> <br> <br>
	      <h3> Surf your own wave, do things different, you never know what can happen until you do it. </h3>
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->