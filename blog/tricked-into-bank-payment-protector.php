<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Don't get tricked into your bank's payment protector / credit card insurance | Gregory Hammond </title>
  <meta name="description" content="You shouldn't have payment protector / credit card insurance on your credit card. It's not worth it.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Don't get tricked into your bank's payment protector / credit card insurance </h3>
        <p>
         You may have noticed on your credit card a line about payment protector or sometimes they call it credit card insurance. Make sure you cancel it. Most of the time they say by activating the credit card you automatically enroll into it, and they may say by keeping it they give you $___ if you become disabled or unemployed.... But in reality the bank won't give you any money because they need money to survive, and you are a customer.
	     <br> <br>
	     If you want to cancel it then the best course of action is to call, give them your information so they can verify it is you. Then the hardest part is listening to them tell you why you should keep it, then tell them you want to cancel. After all that they will tell you that you will receive a letter in the mail to certify that it's cancelled. 
	     <br> <br>
	     It is simple for them to add on, and easy for you to cancel So do it, it took me only 3 minutes to call and cancel.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->