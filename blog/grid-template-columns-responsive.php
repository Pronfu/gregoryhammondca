<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>How to get grid-template-columns to be responsive | Gregory Hammond </title>
  <meta name="description" content="Grid template columns can be tricky to work with, have you got them to be responsive yet?">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> How to get grid-template-columns to be responsive </h3>
        <p>
          <i> Date published: July 23rd 2018 (2018-07-23) </i>
          <br> <br>
          CSS grid is awesome, so awesome that it's the basis for this site. But as with everything you make custom, it doesn't automatically be responsive.
          <br> <br>
          If you have a site that has columns (and you are using grid-template-columns in .container) then you can change whatever is after grid-template-columns, to be auto (and just repeat based on the number of columns you have). 
          <br> <br>
          That's it. It took me many hours to get it, now you can do it quickly.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->