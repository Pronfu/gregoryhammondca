<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Good Job Jeff Bezos for Standing Up Against Extortion | Gregory Hammond </title>
  <meta name="description" content="Jeff Bezos has decided to stand up against a big bully that wants him to issue public statements.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Good Job Jeff Bezos for Standing Up Against Extortion </h3>
        <p>
        <i> Date published: Febuary 8th 2019 </i>
        <br> <br>
		<i> First a recap of what is leading up to this point, then I will go into the actual post. On January 9th 2019 Bezos <a href="https://twitter.com/JeffBezos/status/1083004911380393985">announced on twitter</a> that he and his wife (MacKenzie Bezos) were getting divorced. On January 10th the National Enquirer published a story (with photos and pictures of texts) about Bezos (Jeff) having a relationship with Lauren Sanchez (who is married). Bezos (going to keep referring to Jeff until I mention otherwise) <a href="https://www.thedailybeast.com/bezos-launches-investigation-into-leaked-texts-with-lauren-sanchez-that-killed-his-marriage">hired a team of private investigators to find out how the National Enquirer got the photos</a>. The National Enquirer talked to Bezos (first verbally then through email) that they would release more photos if the investigation didn't stop. Bezos's lawyers argued that the photos are copyright and the photos themselves aren't newsworthy. The National Enquirer thought otherwise and say that unless Bezos (both Jeff and MacKenzie) issued numerous public statements, they would publish the photos. Bezos at that point said fuck it and isn't going to issue any statment they want, instead he <a href="https://twitter.com/JeffBezos/status/1093643321732464646">published</a> a <a href="https://medium.com/@jeffreypbezos/no-thank-you-mr-pecker-146e3922310f">post on Medium exposing the extortion.</a> Now you are caught up. </i>
		<br> <br>
		Not everyone would do this (both what the National Enquirer did and what Bezos published), but Bezos decided to expose what had happening to him. Bezos did published private information (both the confidential email and the cell numbers) but Medium <a href="https://nordic.businessinsider.com/jeff-bezos-wont-get-paid-for-his-medium-post-2019-2?r=US&IR=T">won't say if the post violated their rules</a>, I think if they took the post down then people would riot against Medium.
		<br> <br>
		Some people have said that it would affect the Amazon stock (I don't see why a stock price would go down based on one person and their private information) and as of the time of writing the stock hasn't been affected very much, according to <a href="https://www.marketwatch.com/investing/stock/amzn/charts">Market Watch</a> (the <a href=https://res.cloudinary.com/doklvodcx/image/upload/f_auto,q_auto/v1549679019/PmsniWLZ_mjnqoe.png">stock price of January 9th was $1,659.4200</a>, the <a href=https://res.cloudinary.com/doklvodcx/image/upload/f_auto,q_auto/v1549679162/hhmk1qrK_jzxhs7.png">stock price on January 10th was $1,656.22</a> (which is a difference of $3.20), the <a href="https://res.cloudinary.com/doklvodcx/image/upload/f_auto,q_auto/v1549679521/eMycVwfx_miiip0.png">stock price of Febuary 7th was $1,614.37</a> (which is a difference of $45.50 compared to January 9th), the <a href="https://res.cloudinary.com/doklvodcx/image/upload/f_auto,q_auto/v1549679914/KHUmji1b_aexzxf.png">stock price on Febuary 8th at 7:59pm EST was $1,588.22</a> (was is a different of $72.20 compared to January 9th). Yes there is a slight dip during those time, but at some times the stock price went up (see <a href="https://res.cloudinary.com/doklvodcx/image/upload/f_auto,q_auto/v1549680217/y9FyyiUf_mmywaz.png">this chart from November 12th to February 8th</a>). 
		<br> <br>
		You do have to be a very bold guy to do this, you could just let it simply die down and hope it doesn't affect you too much. At of the time of writing it is too early to tell what will happen (<a href="https://www.wired.com/story/jeff-bezos-legal-case-national-enquirer/">it's hard to argue that there is a legal case</a>).
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->