<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>The Investors Will Soon Be Coming Back | Gregory Hammond </title>
  <meta name="description" content="When you make money you have to pay back enough to make it a return on investment for all.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> The Investors Will Soon Be Coming Back</h3>
        <p>
        <i> Date published: May 27th 2019 </i>
        <br> <br>
        <i> This is an opinion piece, opinions are of myself at the current time (not past or future) and don't represent any employer (past, present or future) or clients or friends / colleagues.</i>
		<br> <br>
		When companies get money from angel investors, venture capitalist or any other way they will ask for their return on investment. The company can keep on getting money but one day they will either go bankrupt, make enough money to pay all back or be acquired.
		<br> <br>
		It's never nice when companies go bankrupt as that means people have lost their jobs, or what they loved doing.
		<br> <br>
		Paying back huge sums of money will take tons of time but everyone wants their money back within a reasonable time frame, so unless the company is making tons of money they will either go bankrupt or get acquired. 
		<br> <br>
		Getting acquired is somewhat nice, the investors get their money back (sometimes multiple times more),the owners get to keep working on what they love, and the employees will most likely keep their jobs. 
		<br> <br>
		There are positives and negatives to each one, a company should never raise a huge ton of money or it won't be so good (bankrupt or acquired).
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->