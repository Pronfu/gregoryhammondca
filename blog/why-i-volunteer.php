<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why I Volunteer - Think Long Term | Gregory Hammond </title>
  <meta name="description" content="When you had to do those volunteer hours in high school did you ever think they would impact your career? Volunteering can change your career and open you up to new opportunities.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why I Volunteer - Think Long Term </h3>
        <p>
          Think back to high school when you had to do mandatory volunteer hours (at least in Ontario, Canada you have to do 40 volunteer hours within 4 years). Did you ever think that doing that might help you make connections and help you decide what you want to do for the rest of your life? I certainly didn't back then but can see it now. 
	      <br> <br>
	      Doing some outside of high school allowed to do something the school didn't teach, it could be almost anything (the school board had a set of requirements for counting the hours as volunteer hours). I did my volunteer hours in various places, I now wish that I expanded my horizon and did something that I didn't already do so I could learn new things. 
	      <br> <br>
	      Too bad I couldn't make websites (and post them on Github under <a href="https://unlicense.org/">unlicense</a>) and have those hours count as community service. I guess the staff at the school board wanted the hours to be done to help the local community in some way. 
	      <br> <br>
	      If you're in high school or considering volunteering then try many things and find the things you love and try to make that into your career. You don't have to have the same career as everyone else, like I own my own company yet nobody else in my college program has yet to create their own business. Be unique, try new things and don't be afraid to fail.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->