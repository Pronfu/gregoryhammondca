<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Why I Don't Like That Microsoft Has Acquired GitHub | Gregory Hammond </title>
  <meta name="description" content="Microsoft now owns Github, which I think is a bad thing.">

  <?php include('blog-header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('blog-left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Why I Don't Like That Microsoft Has Acquired GitHub  </h3>
        <p>
        <i> Date published: June 4th 2018 </i>
        <br> <br>
          For those unfamilar, Microsoft has acquired Github (see <a href="https://www.bloomberg.com/news/articles/2018-06-03/microsoft-is-said-to-have-agreed-to-acquire-coding-site-github?">this Bloomberg article</a> for more details). I don't like that they have done it, let me explain.
          <br> <br>
          Microsoft is a huge company and has mergered or acquired so many companies (see <a href="https://en.wikipedia.org/wiki/List_of_mergers_and_acquisitions_by_Microsoft#Key_acquisitions">this Wikipedia article</a> for a list of them all) that it's just stupid for one company to own that many other companies.
          <br> <br>
          Right now there are 5 big companies: Apple, Microsoft, Google, Disney and Facebook. I bet Facebook one day is either going to be acquired by one of the other big companies, or they will acquire a big company. Once that happens it's down to the big 4. I don't want what happens in <a href="https://en.wikipedia.org/wiki/The_Circle_(Eggers_novel)">The Circle</a> to be a reality (one big company owning everything).
          <br> <br>
          Since I don't want Microsoft to own everything I do, I'm moving all my open-source stuff from <a href="https://github.com/gregoryhammond">Github</a> to <a href="https://bitbucket.org/Pronfu/">Bitbucket</a>. This transition will be happening over the next week or so.
          <br> <br>
          I encourage anyone who has stuff on Github to move it off, don't put it on Gitlab since so many other people are doing that, find an alternative so stuff is spread out and not owned by one (or two) companies. Find an alternative at <a href="https://alternativeto.net/software/github/">alternativeTo</a>.
        <br>
        <?php include('blog-footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->