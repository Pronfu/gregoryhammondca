<br> <br>
------
<br> <br>
<i> Seize the day. This whole site is under <a href="https://unlicense.org/">UNLICENSE</a> / <a href="https://www.tldrlegal.com/l/cc0-1.0">CC0</a> for you to create, learn from, make money from, or anything like that. </i>
<br>
<a href="/privacy-policy">Privacy Policy</a>