<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Resume / CV for Gregory Hammond</title>
  <meta name="description" content="CV / Resume online of Gregory Hammond">

  <?php include('header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <b> Gregory Hammond </b>
		<br>
		Frontend Web Developer
		<br> <br>
		Location: <a href="https://goo.gl/maps/Kd3HD82w3DyYLbc3A">Ajax, Ontario, Canada</a> (willing to take public transit to nearby towns or city for job). I'm also open to remote opportunities.
		<br> <br>
		Email: <a href="mailto:job[at]gregoryhamond[dot]ca?subject=I've%20got%20a%20job%20for%20you&body=Hello%20Gregory%2C">job[at]gregoryhammond.ca</a>
		<br> <br>
		Resume download: <a href="https://gregoryhammond.ca/GregoryHammondResume.pdf">resume.pdf</a>
		<br> <br>
		Flexible working hours are preferred as I volunteer in the community.
		<br> <br>
		
		<b> Experience </b>
		<br>
		
		January 2017 - Present
		<br>
		Web Developer & Owner at <a href="https://gjdev.ca/?ref=gregorycv">Gregory J Development</a>
		<br>
		I've been doing everything related to the business, from finding new clients to working on their websites, to more. More clients have been WordPress websites but I have also done work on Wix, Weebly and more. I have also done work under other companies (I cannot name the exact websites I have worked on due to NDA).
		<br>
		Lanuages I worked on: PHP, CSS, HTML, Javascript.
		<br> <br>
		
		January 2008 - January 2017
		<br>
		Web Developer / Designer & Technology Support Consultant at Freelance
		<br>
		In all this time I have used various tools and skills to help clients get done what needs to be done. In my spare time I worked on and released open source work (more details can be found at <a href="https://gregoryhammond.ca/bitbucket">/code</a>).
		<br>
		Lanuages I worked on: HTML, CSS, PHP.
		<br> <br>
		
		April 2016 - May 2016
		<br>
		Web Developer Intern (1 month placement) at <a href=https://www.mheducation.ca/">McGraw-Hill Education Canada</a>
		<br>
		Worked on various internal websites (some custom websites, some websites using frameworks, and some WordPress websites).
		<br>
		Worked on internal WordPress website, to create slides for homepage and review changes between 2 internal sites.
		Made sure everything I worked on was responsive and was discussed with team.
		<br>
		Lanuages I worked on: HTML, PHP, Javascript, AJAX, Bootstrap framework.
		<br> <br>
		
		September 2014 - August 2015
		<br>
		Social Media Ambassador at <a href="https://durhamcollege.ca/">Durham College</a>
		<br>
		Chosen to offically represent Durham College out of the student population of over 11,000 students.
		<br>
		Worked for communication & marketing department and utilized various social networks to show off life as a student and share relevant information to becoming a student.
		<br> <br>
		
		<b> Skills </b>
		<br>
		Technical: HTML5, CSS, WordPress, Git, Bootstrap, Javascript, SEO
		<br>
		Non-technical: Able to explain complex things in a non-complex way for everyone to understand.
		
		<br> <br>
		
		<b> Education </b>
		<br>
		April 10th 2019 to April 22nd 2019
		<br>
		Responsive Web Design Certificate (300 hour program) at FreeCodeCamp.
		See certificate at <a href="https://www.freecodecamp.org/certification/rocketexclusive/responsive-web-design">https://www.freecodecamp.org/certification/rocketexclusive/responsive-web-design</a>
		<br> <br>
		
		September 2013 to June 2016
		<br>
		Computer Programmer Analyst at Durham College
		<br>
		See more information about the program at <a href="https://durhamcollege.ca/programs/computer-programmer-analyst-three-year">https://durhamcollege.ca/programs/computer-programmer-analyst-three-year</a>
		
		<br> <br>
		
		If there is anything else you need, or have any question please email me and I would be happy to answer them. 
        <br>
        <?php include('footer.php') ?>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->