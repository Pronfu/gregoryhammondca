<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Gregory Hammond | Website Developer & Designer</title>
  <meta name="description" content="The homepage / website of Gregory Hammond, based in Ajax, Ontario, Canada.">

  <?php include('header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
		Gregory Hammond works on websites, is a WordPress expert, speaker, blogger and volunteer.
		<br> <br>
		Gregory primarily works on WordPress, ClassicPress and custom websites but is always open to learning more. Gregory is the owner of <a href="https://gjdev.ca/?ref=gregoryhammondca">Gregory J Development</a>, his portfolio can be viewed on the <a href="https://gjdev.ca/our-work/?ref=gregoryhammondca">Gregory J Development our work page</a>.
		<br> <br>
		Gregory has worked on WordPress websites for over 6+ years which means he can most likely help you with your WordPress website (best way to reach him is by email). Only paid opportunies please.
		<br> <br>
		When he is not working on websites he is speaking about websites, accessibility, technology and more. He has setup a <a href="/speaking">decidated speaking page if you want more details or to contact him</a>.
		<br> <br>
		Besides that, Gregory also writes blog posts. He primarily writes on the <a href="https://gjdev.ca/blog/?ref=gregoryhammondca">Gregory J Development blog</a> and <a href="./blog/index.php">his own blog</a>. 
		<br> <br>
		Gregory also believes in continuously learning so he tends to <a href="/code">create websites and open-source them</a>. 
		<br> <br>
		Finally, Gregory is also a volunteer in the local community. He volunteers with various non-profit organizations.
        <br> <br> <br>
		<b>Gregory always open to new opportunies (either local or remote), the best way to reach him is through <a href="mailto:hello@gregoryhammond.ca?subject=&body=Hello%20Gregory%2C%0A%0AI%20found%20your%20email%20on%20your%20website%20and%20I%20wanted%20to%20....">email</a>.</b>
        <br>
        <?php include('footer.php') ?>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->