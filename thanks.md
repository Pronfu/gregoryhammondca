Thanks to [Drew DeVault](https://drewdevault.com/), [Kevin Lynagh](https://kevinlynagh.com/) and [David Heinemier Hansson (DHH)](http://david.heinemeierhansson.com/) for inspiring me to do a simple site like this.

Thanks to [Sergey Gavelyuk](https://codeburst.io/css-grid-layout-simple-guide-e0296cf14fe8) for the inital template for this site. See his [codepen](https://codepen.io/serg-gavel/pen/OzBJpa) to see the exact template.

Thanks to WebAIM for the help getting the skip to content link to work. See ["Skip Navigation" Links](https://webaim.org/techniques/skipnav/) and [CSS in Action](https://webaim.org/techniques/css/invisiblecontent/) to create your own skip to content link that is visible to only screen readers or those who only use the keyboard.

Huge thanks to the people behind [Can I use](https://caniuse.com), I referred to that site so many times when making this site.

Thanks to [CSS Tricks](https://css-tricks.com/) for their many articles and especially their [CSS Media Queries & Using Available Space](https://css-tricks.com/css-media-queries/) article.

Thanks [Sitepoint](https://www.sitepoint.com/a-basic-html5-template/) for your basic HTML5 template.

Thanks to [Makeuseof](https://www.makeuseof.com/tag/how-to-create-an-rss-feed-for-your-site-from-scratch/) for the help creating the [rss feed](/blog/feed.xml).

Thanks to [Bajrang](https://stackoverflow.com/users/603128/bajrang) over at [Stack Overflow](https://stackoverflow.com/a/8814534) for the [back link](https://stackoverflow.com/q/8814472).

Thanks to [Alex Cican](https://alexcican.com/post/how-to-remove-php-html-htm-extensions-with-htaccess/) for showing how to remove php extension on a page, and adding a canonical meta tag (<link rel="canonical" href="https://alexcican.com/post/single-post">.

Thanks to [GTMetrix](https://gtmetrix.com) for their guides on pagespeed.

Thanks InMotionHosting for their [guide](https://www.inmotionhosting.com/support/website/htaccess/htaccess-change-index-page) to set an default page in .htaccess.

Thanks to DigitalOcean for their [guide to rewrite URL's](https://www.digitalocean.com/community/tutorials/how-to-rewrite-urls-with-mod_rewrite-for-apache-on-ubuntu-16-04) on Apache. 