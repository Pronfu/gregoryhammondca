<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Privacy Policy | Gregory Hammond </title>
  <meta name="description" content="Not very much is collected about you when you visit gregoryhammond.ca but this policy lays out exactly what is collected and when.">

  <?php include('header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Privacy Policy </h3>
        <p>
		  <i> Last updated on August 13h 2018 </i>
		  <br> <br>
		  I do not use any tracking, paywalling, ads or any sort of tracking that let's me know who you are and pages you visit.
		  <br> <br>
		  I only collect data when you email me. If I need to send you an invoice for something it will be handled by my company (<a href="https://gjdev.ca?ref=gregoryhammondca">Gregory J Development</a>).
		  <br> <br>
		  I will never sell any of your data (since I don't know very much on you).
		  <br> <br>
		  I use Cloudflare to protect the site, and make it faster to people around the world. Cloudflare only keeps your <a href="https://blog.cloudflare.com/what-cloudflare-logs/">data for around 4 hours</a> then deletes it. Cloudflare puts one cookie on your browser to apply security settings on a per-client basis. See their <a href="https://support.cloudflare.com/hc/en-us/articles/200170156-What-does-the-Cloudflare-cfduid-cookie-do-">support article</a> for more details.
		  <br> <br>
		  HTML5Shiv (which is used to help Internet Explorer understand HTML5) is hosted  by <a href="https://www.jsdelivr.com/">jsdelivr.com</a>, see their <a href="https://www.jsdelivr.com/privacy-policy-jsdelivr-net">privacy policy</a> for what they track when we use their cdn.
		  <br> <br>
		  There may be links to third-party sites, I am not responsible for anything that goes on while you are on those sites. Please read over their privacy policy to see what they track about you.
		  <br> <br>
		  For your privacy I have disable referral, so if you click to another site they will not know that you have came from this site.
		  <br> <br>
		  There may be images placed on this site, I use <a href="https://cloudinary.com">Cloudinary</a> for image hosting. 
        <br>
        <?php include('footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->