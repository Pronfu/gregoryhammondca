<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Bitbucket | Gregory Hammond </title>
  <meta name="description" content="This is an overview of my public bitbucket repositories. Most repos are made using PHP, HTML5, Javascript.">

  <?php include('header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Bitbucket </h3>
        <p>
        Here is the highlights of my some public bitbucket repositories. For a full list of everything on my bitbucket then visit <a href="https://bitbucket.org/Pronfu">https://bitbucket.org/Pronfu</a>. This is accurate as of January 2nd 2020.
        <br> <br>
        <a href="https://elastic-heisenberg-f98d72.netlify.com/">Crypto Chart</a> - A easy to use, responsive and privacy-friendly site to convert popular cryptocurrencies to USD. <a href="https://bitbucket.org/Pronfu/cryptochart">View code</a>
        <br> <br>
        <a href="https://vigilant-kirch-398a09.netlify.com/">Members Site</a> - A site for your members to take courses, talk in the forums, see who else is a member and more. <a href="https://bitbucket.org/Pronfu/memberssite">View code</a>
        <br> <br>
        <a href="https://vigorous-wescoff-19aac5.netlify.com/">Event Listings</a> - Instead of using a site like Meetup or Eventbrite for your event, why not have your event listings and RSVP on your own website. <a href="https://bitbucket.org/Pronfu/eventlisting">View code</a>
        <br> <br>
        <a href="https://silly-sammet-8284d4.netlify.com/">cleanAmazon</a> - This is a clean / minimalist version of Amazon.<a href="https://bitbucket.org/Pronfu/cleanamazon">View code</a>
		<br> <br>
		<a href="https://gregoryhammond.ca">gregoryhammondca</a> - This is the repo for this website, to be able to show you the code and have a backup. <a href="https://bitbucket.org/Pronfu/gregoryhammondca">View code</a>
		<br> <br>
		<a href="https://romantic-banach-bb1676.netlify.com/">styleSite</a> - This is a live website based on a design from dribbble. <a href="https://bitbucket.org/Pronfu/stylesite">View code</a>
		<br> <br>
		<a href=https://modern-identity-landing-page.netlify.com/"">modern-identity-landing-page</a> - A landing page that is modern (made with <a href="https://developer.mozilla.org/en-US/docs/Glossary/Flexbox">flexbox</a> and should work with the last 10 versions of browsers. <a href="https://bitbucket.org/Pronfu/modern-identity-landing-page">View code</a>
        <br> <br>
        <a href="https://random-techmeme-page.netlify.com/">Random Techmeme Page</a> - This project generates a random techmeme page for you to visit and look back at what has changed in technology. <a href="https://bitbucket.org/Pronfu/random-techmeme-page">View code</a>
        <br> <br>
        <a href="https://trusting-tereshkova-fecd66.netlify.com/">website-key-detection</a> - Use javascript to detect if you type in a certain key combo then something will happen. To view what the site goes to <a href="https://trusting-tereshkova-fecd66.netlify.com/">https://trusting-tereshkova-fecd66.netlify.com/</a> and once loaded enter github (as you would normally type) and see what's set to happen. <a href="https://bitbucket.org/Pronfu/website-key-detection">View code</a>. 
        <br> <br>
        <a href="https://bitbucket.org/Pronfu/bootstrap-starter-wp-theme">bootstrap-starter-wp-theme</a> - My first WordPress theme created using <a href="https://blog.getbootstrap.com/2017/01/06/bootstrap-4-alpha-6/">Bootstrap v4.0.0-alpha 6</a> & <a href="https://necolas.github.io/normalize.css/">Normalize.css</a>.
        <br> <br>
        <a href="https://bitbucket.org/Pronfu/21st-century-toronto">21st-century-toronto</a> - Created a 21st century version of toronto.ca, using <a href="https://blog.getbootstrap.com/2017/01/06/bootstrap-4-alpha-6/">Bootstrap v4 alpha 6</a>. 
        <br> <br>
        <a href="https://bitbucket.org/Pronfu/wp-town-theme">wp-town-theme</a> - Created a WordPress theme that can be used by a town of city. Includes <a href="https://blog.getbootstrap.com/2018/04/30/bootstrap-4-1-1/">Bootstrap 4.1.1</a>, <a href="https://blog.fontawesome.com/font-awesome-5-0-13-the-quest-for-requests-bae8eb2a35f8">Font Awesome 5.0.13</a>, <a href="https://simple.wikipedia.org/wiki/JQuery">jQuery 3.3.1</a>, and <a href="https://modernizr.com/">Modernizr 2.8.3</a> (all hosted on <a href="https://www.cloudflare.com/learning/cdn/what-is-a-cdn/">cdn's</a>).
        <br> <br>
        <a href="https://bitbucket.org/Pronfu/javascript-cube-examples">javascript-cube-examples</a> - Created moving cubes (slow, medium and fast speed) using <a href="https://threejs.org/">three.js (Javascript 3D library)</a>. 
        <br> <br>
        <a href="https://relaxed-shaw-620466.netlify.com/">html5-paint-canvas</a> - Painting in your browser window using HTML5. Should work in Internet Explorer (espically 8 & 9) since I have added <a href="https://en.wikipedia.org/wiki/HTML5_Shiv">HTML5Shiv</a> and <a href="http://selectivizr.com/">selectivizr</a>. <a href="https://bitbucket.org/Pronfu/html5-paint-canvas">View code</a>
        <br> <br>
        <a href="https://admiring-fermat-d12829.netlify.com/">css-variable-in-js</a> - Change CSS variables using Javascript.<a href="https://bitbucket.org/Pronfu/css-variable-in-js">View code</a>
        <br> <br>
        <a href="https://bitbucket.org/Pronfu/railsblogfaqmanager">RailsBlogFaqManager</a> - The first project I made using Ruby on Rails, it is under <a href="https://bitbucket.org/Pronfu/railsblogfaqmanager/src/master/license.txt">GLWTPL (Good Luck With That Public License)</a>.
        <br> <br>
        <a href="https://10kb.netlify.com/">10kb-website</a> - A website that is under 10kb in size. Framework is <a href="https://mincss.com/">min</a>. <a href="https://bitbucket.org/Pronfu/10kb-website">View code</a>
        <br> <br>
        <a href="https://sad-einstein-84deb8.netlify.com/">js-drum-kit</a> - Drum kit created Javascript.<a href="https://bitbucket.org/Pronfu/js-drum-kit">View code</a>
        <br> <br>
        <a href="https://bitbucket.org/Pronfu/december-wordpress-theme">December-wordpress-theme</a> - A December WordPress theme for a blog.
        <br> <br>
        <a href="https://loving-mclean-de07c8.netlify.com/">js-css-clock</a> - A real time clock made using HTML, CSS, and Javascript. <a href="https://bitbucket.org/Pronfu/js-css-clock">View code</a>
        <br>
        <?php include('footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->