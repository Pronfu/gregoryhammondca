# Changelog
All notable changes to this project will be documented in this file.

This is a human-readable version of committs, to show what has changed.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## [1.0.3.7] - 2018-10-26

### Added
- blog post -> Why I Don't Pay Attention To Apple Events Anymore

### Edited
- feed.xml & blog index.php -> add blog post

## [1.0.3.6] - 2018-10-07

### Added
- blog post -> Devhub toronto review

### Edited
- Privacy policy page
- Support page
- feed.xml & blog index.php -> add blog post

## [1.0.3.5] - 2018-09-05

### Added
- blog post -> Why I Don't Have A Uses Page

### Edited
- feed.xml -> add blog post
- blog index.php -> add blog post

## [1.0.3.4] - 2018-09-02

### Added
- blog post -> Why Choose JsDelivr Over Cdnjs

### Edited
- feed.xml -> add blog post
- blog index.php -> add blog post

## [1.0.3.3] - 2018-08-18

### Changed
- htaccess -> redirect www to non-www
- Time to stop saying "Sorry for the delayed response" blog post -> change image locations
- privacy policy -> change where images may be hosted

## [1.0.3.2] - 2018-08-13

### Added
- blog post -> Time to stop saying "Sorry for the delayed response" 

### Changed
- feed.xml, blog home page -> to add this blog post and to understand that all blog posts on this site are mine and mine alone
- privacy policy -> I may use imaegs on this site (on an image host that hopefully doesn't log)

## [1.0.3.1] - 2018-08-04

### Added
- blog post -> Why I'm Using PGP (Pretty Good Privacy) Again

### Changed
- feed.xml & blog home page -> added recent blog post

## [1.0.3] - 2018-08-03

### Added
- pgp page (pgp.md)

### Changed
- left sidebar -> to include link to pgp page

## [1.0.2.2] - 2018-08-02

### Changed
- blog homepage -> point to correct <a href="/blog/turn-off-email-notifications.php">I turned off email notifications, and it has increased my productivity</a> blog post
- left -> changed /bitbucket to /code

## [1.0.2.1] - 2018-08-01

### Added
- old blog posts
- bitbucket page - to explain what each project on bitbucket is
- examples folder -> Redirect to root 404 page

### Changed
- blog main page to include old blog post
- sitemap -> to include old blog post & bitbucket page
- left -> to link to bitbucket page rather than bitbucket site directly (as suggested by <a href="http://loufranco.com/blog/resume-tip-link-to-yoursite-github">Lon Franco</a>)
- blog/htaccess -> Change 404 page to root page
- htaccess -> Added redirect from /code to /bitbucket

## [1.0.2] - 2018-07-29

### Added
- Changelog (the exact file you are reading)

### Changed
- Readme -> added [CC0 license](https://www.tldrlegal.com/l/cc0-1.0)
- footer, added that site is under [unlicense](https://unlicense.org/) and [CC0](https://www.tldrlegal.com/l/cc0-1.0)

## [1.0.1] - 2018-07-27

### Changed
- Privacy policy -> updated date, cloudflare puts a cookie on your browser, added link to jsdelivr

## [1.0.0] - 2018-07-26
Live on Website

### Added
- Favicon (png, svg)

### Changed
- htaccess - redirect some old blog posts
- header & blog header - change location to favicon
- css -> from % to fr (right side takes up 2/3 of screen, left takes 1/3)
- thanks -> digitalocean for their guide to rewrite url (as I had not turned on apache mod rewrite on server)

## [0.0.7] - 2018-07-25

### Added
- 404 page
- Less is more page (not a blog)
- Not hosting examples page

### Changed
- htaccess - Redirect /feed to /blog/feed, set default file & timezone, set 404 page, disable image hotlinking, force UTF-8
- robots.txt - disallow certain pages (404, footer, header, /css, /font), point to sitemap
- sitemap - remove .php from pages, add less is more & why no wordpress page

## [0.0.6] - 2018-07-25

### Added
- Default blog page

### Changed
- Footer - show where page ends and footer begins
- feed - added why no wordpress blog post
- blog - why-i-dont-provde-my-phone-number-in-my-email-signature.php to why-i-dont-provide-my-phone-number-in-email-signature.php (contents unchanged)
- Footer - remove .php from privacy policy
- Home page - changed blog link to /blog
- Privacy policy - Update to include correct html and accessibility
- Support - Update to include correct html

### Removed
- Do not enjoy being tracked blog post, Reason: Not finished yet


## [0.0.5] - 2018-07-24

### Added
- htaccess
- old blog posts
- CSS 100% width minify
- Header -> A lot from http://gethead.info/
- Robots.txt -> Disallow blog template
- Sitemap

### Changed
- CSS (100% width) grid-template-columns to auto auto
- Homepage -> Proper html5
- Privacy policy -> Complete sentences.
- Donations -> Now called support page
- Thanks.md -> Added more links

## [0.0.4] - 2018-07-15

### Added
- Left sidebar for blog
- minify "skip to content" & css for grid
- Css to show 100% width when screen is small
- Locally hosted icons
- Donations page

## [0.0.3] - 2018-07-07

### Added
- Privacy policy
- Analystics (Clicky)
- Default css for unvisited link, visited link, link focus, underline and more.

### Changed
- Fonts (now using default fonts, for privacy and speed)
- Moved stylesheets to header.php
- Removed !important from css

## [0.0.2] - 2018-07-03

### Added
- Thanks, and Unlicense
- Footer (footer.php)
- "Skip to content" link

### Changed
- Using CSS Grid now for basis of site, rather than a framework.
- Made site responsive (% instead of px)
- Homepage (added what I am doing now, where I volunteer)

### Removed
- XMPP and twitter from left side
- What I may not respond to, what I won't respond to

## [0.0.1] - 2018-07-03

### Added
- Added project to bitbucket from github (because Microsoft owns Github, see my post on [dev.to](https://dev.to/gregory/why-i-dont-like-that-microsoft-has-acquired-github-368k) for more details)