<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Speaking | Gregory Hammond </title>
  <meta name="description" content="Gregory Hammond is available to speak on topics of websites, entrepreneurs, and Crohn's Disease.">

  <?php include('header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Speaking </h3>
        <p>
         I talk about websites, entrepreneurship, Crohn's Disease and other topics related to technology. This page and information is for anyone who is interested in inviting me to speak. If you have any questions or wish to request for me to speak at your event please <a href="mailto:speaking@gregoryhammond.ca?subject=Request%20to%20speak&body=I%20would%20like%20you%20to%20speak%20at%20____(event%20name)%2C%20it%20is%20on%20____(event%20date)%2C%20it%20will%20be%20at%20____(event%20location).%20%0A%0AWe%20would%20like%20you%20to%20speak%20on%20____(what%20you%20want%20Gregory%20to%20speak%20on).%0A%0AWe%20can%20pay%20you%20___(payment)%20for%20you%20to%20speak.%0A%0AMy%20contact%20information%20is%3A%0A(your%20name)%0A(your%20email%20address)%0A(your%20phone%20number%20including%20extension).%0A%0AThanks.">email me</a>.
		 <br> <br>
		 I can speak in-person, over video conference or even via phone. 
		 <br> <br>
		 If I speak in-person I ask that I have a microphone (either lavalier or handheld as I can't have a microphone that goes on or over ears) to ensure everyone can hear me. If I'm speaking via video conference I have a headset that I will use. 
		 <br> <br>
		 The length of the speaking engagement can change based on your needs (please have a countdown timer to ensure I stay on time). There can be a question and answer session at the end of every talk, if you allow.
		 <br> <br>
		 I encourage you to record my talk and share it publicly so I can share it.
		 <br> <br>
		 <b>Availability:</b>
		 <br> <br>
		 <ul>
		 <li> Speaking isn't my full-time job, but since I'm self-employed I have the flexibility to base my schedule around your event. The sooner you email me, the greater chance there is that I'm available to speak. But I do require time to work on the presentation before your event, as I tweak each presentation based on your attendees to ensure they fully understand the content.</li>
		 <br> <br>
		 <li>I will be there before my talk to setup, as well as will stay after to pack up my stuff and chat with attendees. If you wish for me to be apart of anything additional (panel, doing a Q&A session, having "office hours" with attendees, etc...) please let me know ahead of time so I can plan my schedule accordingly.</li>
		 <br> <br>
		 <li>Please don't ask me to stay for your entire event, the answer by default will be no.</li>
		 </ul>
		 <br> <br>
		 <b>Covering Expenses:</b>
		 <br>
		 <ul>
		 <li>I do charge a speaking fee, this fee does vary based on numerous factors (the length of the talk, the length that I'm being asked to stay and more). I will sometimes waive my fee for <a href="https://toastmasters.org">Toastmasters clubs</a> (I'm a member of Toastmasters), community events or other non-profit events. Please be up front if you can't pay me, it will save us both tons of time.</li>
		 <br> <br>
		 <li>If you want me to talk in-person then I will charge a travel fee. If the event is easily accessible via public transit then this travel fee will be less (enough to cover public transit costs to and from the event), but if the venue isn't easily accessible using public transit then I will charge a per-km fee (I will most likely be using some sort of ride-share). </li>
		 <br> <br>
		 <li>I would prefer to have these taken care of up front (I can send you an invoice) rather than be reimbursed but I understand if being reimbursed works better.</li>
		 </ul>
		 <br> <br>
		 <b>Setup and information:</b>
		 <br> 
		 I expect to present from my own laptop. I will have a Windows laptop (with VGA), if you don't have a VGA connection please provide the cables so I can connect. If the venue doesn't allow someone to connect their own laptop then I can send my presentation to whoever needed.
		 <br> <br>
		 <b>Let me know in good time:</b>
		 <ul>
		 <li> The expected audience for the event. Tell me about the audience, their technical background, whether they are mostly entrepreneurs, from large companies and so on.</li>
		 <br>
		 <li> Slide resolution and any other technical details that will be help when I prepare.</li>
		 <br>
		 <li> If there will be internet (wired or wireless) as I may have videos or things online that I wish to show off. If you don't have internet that is fine, I just want to know ahead of time. Bonus points if there is a password-protected internet just for speakers.</li>
		 <br>
		 <li> If you will have somewhere that speakers can chill and recharge so that I can give 100% when I am speaking.</li>
		 </ul>
		 <br> <br>
		 <b>Presentation Ideas:</b>
		 <br>
		 I have various things I can speak on, these are just some of them. If you have another idea for a talk that is similar to these then let me know.
		 <br>
		 <ul>
		 <li>The x tips that are needed on every website (the x is a number and can be changed based on how long I have)</li>
		 <br>
		 <li>Website accessibility</li>
		 <br>
		 <li>How Crohn's disease almost killed me</li>
		 <br>
		 <li>Online privacy</li>
		 <br>
		 </ul>
		 <br> <br>
		 <b> Past Speaking Engagements:</b>
		 <br> <br>
		 <a href="https://www.youtube.com/watch?v=SUl19KTYvzM">Look On Up (To Mentors)</a> - October 2019
		 <br> <br>
		 <a href="https://www.youtube.com/watch?v=Mv0dQghi6_Q">3 Tips For Your Toastmasters Club Website (5 minutes)</a> - August 2019
		 <br> <br>
		 <a href="https://speakerdeck.com/gregoryca/social-media-how-to-use-it-to-attract-more">Social Media - How to Use It to Attract More</a>, presented to members of <a href="https://www.toastmasters60.com/">Toastmasters District 60</a> - September 2018
		 <br> <br> <br>
		 I regularly speak at my Toastmasters club and other Toastmasters clubs in the area (these speeches are about almost anything, and the time can vary from 5 minutes to 30 minutes).
		 
		 <br> <br> <br>
		 <b> If you are interested in having me speak please <a href="mailto:speaking@gregoryhammond.ca?subject=Request%20to%20speak&body=I%20would%20like%20you%20to%20speak%20at%20____(event%20name)%2C%20it%20is%20on%20____(event%20date)%2C%20it%20will%20be%20at%20____(event%20location).%20%0A%0AWe%20would%20like%20you%20to%20speak%20on%20____(what%20you%20want%20Gregory%20to%20speak%20on).%0A%0AWe%20can%20pay%20you%20___(payment)%20for%20you%20to%20speak.%0A%0AMy%20contact%20information%20is%3A%0A(your%20name)%0A(your%20email%20address)%0A(your%20phone%20number%20including%20extension).%0A%0AThanks.">email me</a> with details of your event. </b>
        <br>
        <?php include('footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->