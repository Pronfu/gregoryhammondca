<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Page Not Found | Gregory Hammond </title>
  <meta name="description" content="Sorry but the page you are looking for can't be found.">

  <?php include('header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Page Not Found </h3>
        <p>
         I'm sorry but the page you are looking for can't be found, please make sure you typed in the page correctly and if you still can't get to the page please email me (email address will be on the left side or on top) telling me what page you are looking for.
        <br>
        <?php include('footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->