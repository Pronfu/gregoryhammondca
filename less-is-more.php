<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Less Is More | Gregory Hammond </title>
  <meta name="description" content="Post less, do work done and don't be distracted by social media.">

  <?php include('header.php') ?>

</head>

  <body> 
   <div id="accessibility"> <a href="#main">Skip to main content</a> </div>
    <div class="container">
     <div class="item">
      <?php include('left.php') ?>
     </div> <!-- end item -->
     <div class="item">
      <main id="main">
        <h3> Less Is More </h3>
        <p>
         Social media wants us to spend all day on them, and they will do almost anything to do that. I have decided to spend less time on social media. I will be checking all my social media accounts up to 3 times per day.
         <br> <br>
         If you need to get in touch with me, that can't wait until I check social media please email me (email address is on the left hand side).
        <br>
        <?php include('footer.php') ?>
        </p>
     </div> <!-- end item -->
    </div> <!-- end container -->
   </body> <!-- end body -->
</html> <!-- end html -->