<meta name="author" content="Gregory Hammond">

<!-- Theme Color for Chrome, Firefox OS and Opera -->
<meta name="theme-color" content="#7EC0EE">

<!-- Control the behavior of search engine crawling and indexing -->
<meta name="robots" content="index,follow"><!-- All Search Engines -->
<meta name="googlebot" content="index,follow"><!-- Google Specific -->

<!-- Verify website ownership -->
<meta name="google-site-verification" content="FTscB9S2yjUhnlVND5ElW6DSMBCsXJbsDRR2lEU9CYQ" /><!-- Google Search Console -->
<meta name="msvalidate.01" content="B5EE3901B43DCC98ACA4A1568ECF502F" /><!-- Bing Webmaster Center -->
<meta name="norton-safeweb-site-verification" content="5fijcm-k2zjj5hq-mn5kouf42c7i8gms0vx4evwmwdrrjkdlmhjqmdynlw30fm804da5rnaidsxoowtixg4bjmsr321sn9sz9mxdn6ow7ga74xwjv270cj84jrrsmzbi" /><!-- Norton Safe Web -->
<meta name="have-i-been-pwned-verification" value="efadeafa432f31b1b8962ba14c04a2cc">

<!-- Identify the software used to build the document -->
<meta name="generator" content="CSS Grid">

<!-- Don't tell another site that you came from this site -->
<meta name="referrer" content="no-referrer">

<!-- Disable automatic detection and formatting of possible phone numbers -->
<meta name="format-detection" content="telephone=no">

<!-- Links to information about the author(s) of the document -->
<link rel="author" href="humans.txt">

<!-- Refers to a copyright statement that applies to the link's context -->
<link rel="license" href="UNLICENSE.md">

<!-- RSS Feed -->
<link rel="alternate" type="application/rss+xml" title="Subscribe to Gregory Hammond's Blog" href="/blog/feed.xml" />


<link rel="stylesheet" media='screen and (min-width: 1px) and (max-width: 746px') href="css/grid.full-width.min.css" />
<link rel="stylesheet" media='screen and (min-width: 746px) and (max-width: 8000px') href="css/grid.min.css" />
<link rel="stylesheet" type="text/css" href="css/accessibility.min.css">
<link rel="stylesheet" type="text/css" href="css/icons.css"> <!-- contains @font-face and character code definitions for icons -->

<!-- IE Support -->

<!--[if IE]>
<script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
<![endif-->

<!-- IE 7 support for icons on left side -->
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="css/icons-ie7.css">
<![endif]-->

<!-- Favicons -->

<!-- For IE 10 and below -->
<!-- Place favicon.ico in the root directory - no tag necessary -->

<!-- Icon in the highest resolution we need it for -->
<link rel="icon" sizes="192x192" href="favicon.png">

<!-- Apple Touch Icon (reuse 192px icon.png) -->
<link rel="apple-touch-icon" href="favicon.png">

<!-- Safari Pinned Tab Icon -->
<link rel="mask-icon" href="favicon.svg" color="blue">
